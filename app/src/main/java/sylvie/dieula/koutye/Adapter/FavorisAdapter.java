package sylvie.dieula.koutye.Adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import sylvie.dieula.koutye.Interface.ItemClickListener;
import sylvie.dieula.koutye.Models.Favoris;
import sylvie.dieula.koutye.R;

class FavorisViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView image;
    public TextView text;
    public FloatingActionButton button;

    private ItemClickListener itemClickListener;


    public FavorisViewHolder(View itemView) {

        super(itemView);
        image = (ImageView) itemView.findViewById(R.id.ivFavoris);
        text = (TextView) itemView.findViewById(R.id.tvDescription);
        button = (FloatingActionButton) itemView.findViewById(R.id.btn_favorites);
    }

    @Override
    public void onClick(View view) {

    }
}

public class FavorisAdapter extends RecyclerView.Adapter<FavorisViewHolder>{

public FavorisAdapter(List<Favoris> listData, Context context) {
        this.listData = listData;
        this.context = context;
        }

private List<Favoris> listData = new ArrayList<>();
private Context context;


@NonNull
@Override
public FavorisViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.item_favoris,parent,false);
        return  new FavorisViewHolder(itemView);
        }

@Override
public void onBindViewHolder(@NonNull FavorisViewHolder holder, int position) {



        }

@Override
public int getItemCount() {
        return listData.size();
        }
}
