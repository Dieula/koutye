package sylvie.dieula.koutye.Models;

import java.util.List;

public class User {

    private String nomAgence;
    private String passord;
    private String email;
    private String telephone;

/*    private String latLong;*/

    List<Acheter> achat;
    List<Louer> location;
    List<Sejour> sejourne;
    List<Gestion> gestions;

    public User() {
    }

    public User(String nomAgence, String passord, String email, String telephone) {
        this.nomAgence = nomAgence;
        this.passord = passord;
        this.email = email;
        this.telephone = telephone;
        this.achat = achat;
        this.location = location;
        this.sejourne = sejourne;
        this.gestions = gestions;
    }

    public String getNomAgence() {
        return nomAgence;
    }

    public void setNomAgence(String nomAgence) {
        this.nomAgence = nomAgence;
    }

    public String getPassord() {
        return passord;
    }

    public void setPassord(String passord) {
        this.passord = passord;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public List<Acheter> getAchat() {
        return achat;
    }

    public void setAchat(List<Acheter> achat) {
        this.achat = achat;
    }

    public List<Louer> getLocation() {
        return location;
    }

    public void setLocation(List<Louer> location) {
        this.location = location;
    }

    public List<Sejour> getSejourne() {
        return sejourne;
    }

    public void setSejourne(List<Sejour> sejourne) {
        this.sejourne = sejourne;
    }

    public List<Gestion> getGestions() {
        return gestions;
    }

    public void setGestions(List<Gestion> gestions) {
        this.gestions = gestions;
    }
}
