package sylvie.dieula.koutye.Models;

import java.io.Serializable;

public class Proprietaire implements Serializable{

    private String nomComplet;
    private String proprieaireID;
    private String email;
    private String telephone;
    private String adresse;

    private String piecePhoto;

    public Proprietaire(String proprieaireID) {
    }

    public Proprietaire(String nomComplet, String proprieaireID, String email, String telephone, String adresse, String piecePhoto) {
        this.nomComplet = nomComplet;
        this.proprieaireID = proprieaireID;
        this.email = email;
        this.telephone = telephone;
        this.adresse = adresse;
        this.piecePhoto = piecePhoto;
    }

    public String getProprieaireID(String proprietaireID) {
        return proprieaireID;
    }

    public void setProprieaireID(String proprieaireID) {
        this.proprieaireID = proprieaireID;
    }

    public String getNomComplet() {
        return nomComplet;
    }

    public void setNomComplet(String nomComplet) {
        this.nomComplet = nomComplet;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPiecePhoto() {
        return piecePhoto;
    }

    public void setPiecePhoto(String piecePhoto) {
        this.piecePhoto = piecePhoto;
    }
}
