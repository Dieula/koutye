package sylvie.dieula.koutye.Models;

import java.io.Serializable;

public class Sejour implements Serializable {
    private String image;
    private String SejourID;
    private String type;
    private String description;
    private String price;
    private String mesure;
    private String title;
    private String agence;
    private String adresse;
    private String chambre;
    private String piece;
    private String dureMin;
    private String dureMax;
    private String latLong;
    private String telephone;
    private String userId;

    public Sejour() {
    }

    public Sejour(String image, String sejourID, String type, String description, String price, String mesure, String title, String agence, String adresse, String chambre, String piece, String dureMin, String dureMax, String latLong, String telephone, String userId) {
        this.image = image;
        SejourID = sejourID;
        this.type = type;
        this.description = description;
        this.price = price;
        this.mesure = mesure;
        this.title = title;
        this.agence = agence;
        this.adresse = adresse;
        this.chambre = chambre;
        this.piece = piece;
        this.dureMin = dureMin;
        this.dureMax = dureMax;
        this.latLong = latLong;
        this.telephone = telephone;
        this.userId = userId;
    }

    public String getSejourID() {
        return SejourID;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSejourID(String sejourID) {
        return SejourID;
    }

    public void setSejourID(String sejourID) {
        SejourID = sejourID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getMesure() {
        return mesure;
    }

    public void setMesure(String mesure) {
        this.mesure = mesure;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAgence() {
        return agence;
    }

    public void setAgence(String agence) {
        this.agence = agence;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getChambre() {
        return chambre;
    }

    public void setChambre(String chambre) {
        this.chambre = chambre;
    }

    public String getPiece() {
        return piece;
    }

    public void setPiece(String piece) {
        this.piece = piece;
    }

    public String getDureMin() {
        return dureMin;
    }

    public void setDureMin(String dureMin) {
        this.dureMin = dureMin;
    }

    public String getDureMax() {
        return dureMax;
    }

    public void setDureMax(String dureMax) {
        this.dureMax = dureMax;
    }

    public String getLatLong() {
        return latLong;
    }

    public void setLatLong(String latLong) {
        this.latLong = latLong;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
}