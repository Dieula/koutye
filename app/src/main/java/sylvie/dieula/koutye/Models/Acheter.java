package sylvie.dieula.koutye.Models;

import android.widget.TextView;

import java.io.Serializable;
import java.util.List;

public class Acheter implements Serializable {

    private String image;
    private String acheterID;
    private String type;
    private String description;
    private String price;
    private String mesure;
    private String telephone;
    private String agence;
    private String adresse;
    private String chambre;
    private String piece;
    private String latLong;
    private String userId;

    public Acheter() {
    }

    public Acheter(String image, String acheterID, String type, String description, String price, String mesure, String telephone, String agence, String adresse, String chambre, String piece, String latLong, String userId) {
        this.image = image;
        this.acheterID = acheterID;
        this.type = type;
        this.description = description;
        this.price = price;
        this.mesure = mesure;
        this.telephone = telephone;
        this.agence = agence;
        this.adresse = adresse;
        this.chambre = chambre;
        this.piece = piece;
        this.latLong = latLong;
        this.userId = userId;
    }

    public String getAcheterID() {
        return acheterID;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAcheterID(String acheterID) {
        return this.acheterID;
    }

    public void setAcheterID(String acheterID) {
        this.acheterID = acheterID;
    }

    public String getType() {
        return type;
    }


    public String getDescription() {
        return description;
    }



    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getMesure() {
        return mesure;
    }

    public void setMesure(String mesure) {
        this.mesure = mesure;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getAgence() {
        return agence;
    }

    public void setAgence(String agence) {
        this.agence = agence;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getChambre() {
        return chambre;
    }

    public void setChambre(String chambre) {
        this.chambre = chambre;
    }

    public String getPiece() {
        return piece;
    }

    public void setPiece(String piece) {
        this.piece = piece;
    }

    public String getLatLong() {
        return latLong;
    }

    public void setLatLong(String latLong) {
        this.latLong = latLong;
    }

}
