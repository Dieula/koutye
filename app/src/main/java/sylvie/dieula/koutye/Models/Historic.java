package sylvie.dieula.koutye.Models;

import java.io.Serializable;
import java.util.List;

public class Historic implements Serializable {

    private  String idUser;
    private  String image;
    private  String idCategorie;
    private List<Acheter> acheters;
    private List<Louer> louers;
    private List<Sejour> sejours;
    private List<Gestion> gestions;

    public Historic() {
    }

    public Historic(String idUser, String image, String idCategorie, List<Acheter> acheters, List<Louer> louers, List<Sejour> sejours, List<Gestion> gestions) {
        this.idUser = idUser;
        this.image = image;
        this.idCategorie = idCategorie;
        this.acheters = acheters;
        this.louers = louers;
        this.sejours = sejours;
        this.gestions = gestions;
    }

    public String getIdCategorie() {
        return idCategorie;
    }

    public void setIdCategorie(String idCategorie) {
        this.idCategorie = idCategorie;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<Acheter> getAcheters() {
        return acheters;
    }

    public void setAcheters(List<Acheter> acheters) {
        this.acheters = acheters;
    }

    public List<Louer> getLouers() {
        return louers;
    }

    public void setLouers(List<Louer> louers) {
        this.louers = louers;
    }

    public List<Sejour> getSejours() {
        return sejours;
    }

    public void setSejours(List<Sejour> sejours) {
        this.sejours = sejours;
    }

    public List<Gestion> getGestions() {
        return gestions;
    }

    public void setGestions(List<Gestion> gestions) {
        this.gestions = gestions;
    }
}
