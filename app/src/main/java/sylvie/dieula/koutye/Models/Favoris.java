package sylvie.dieula.koutye.Models;

public class Favoris {

    private  String KayCategory;
    private  String Image;
    private  String KayName;
    private  String ImageFavoris;
    private  String UserPhone;
    private  String acheterID;
    private  String louerID;
    private  String SejourID;
    private  String gererID;


    public Favoris(String acheterID, String image, String description, String adresse, String agence, String acheterCurrentDescription) {
    }

    public Favoris(String kayCategory, String image, String kayName, String imageFavoris, String userPhone, String acheterID, String louerID, String sejourID, String gererID) {
        KayCategory = kayCategory;
        Image = image;
        KayName = kayName;
        ImageFavoris = imageFavoris;
        UserPhone = userPhone;
        this.acheterID = acheterID;
        this.louerID = louerID;
        SejourID = sejourID;
        this.gererID = gererID;
    }

    public String getAcheterID() {
        return acheterID;
    }

    public void setAcheterID(String acheterID) {
        this.acheterID = acheterID;
    }

    public String getLouerID() {
        return louerID;
    }

    public void setLouerID(String louerID) {
        this.louerID = louerID;
    }

    public String getSejourID() {
        return SejourID;
    }

    public void setSejourID(String sejourID) {
        SejourID = sejourID;
    }

    public String getGererID() {
        return gererID;
    }

    public void setGererID(String gererID) {
        this.gererID = gererID;
    }

    public String getUserPhone() {
        return UserPhone;
    }

    public void setUserPhone(String userPhone) {
        UserPhone = userPhone;
    }

    public String getKayCategory() {
        return KayCategory;
    }

    public void setKayCategory(String kayCategory) {
        KayCategory = kayCategory;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getKayName() {
        return KayName;
    }

    public void setKayName(String kayName) {
        KayName = kayName;
    }

    public String getImageFavoris() {
        return ImageFavoris;
    }

    public void setImageFavoris(String imageFavoris) {
        ImageFavoris = imageFavoris;
    }
}
