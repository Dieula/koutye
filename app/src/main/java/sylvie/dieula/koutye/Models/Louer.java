package sylvie.dieula.koutye.Models;

import java.io.Serializable;

public class Louer implements Serializable {

    private String image;
    private String louerID;
    private String type;
    private String description;
    private String price;
    private String mesure;
    private String title;
    private String agence;
    private String adresse;
    private String chambre;
    private String piece;
    private String dure;
    private String telephone;
    private String userId;

    private String latLong;

    User user;

    public Louer() {
    }


    public Louer(String image, String louerID, String type, String description, String price, String mesure, String title, String agence, String adresse, String chambre, String piece, String dure, String telephone, String userId, String latLong, User user) {
        this.image = image;
        this.louerID = louerID;
        this.type = type;
        this.description = description;
        this.price = price;
        this.mesure = mesure;
        this.title = title;
        this.agence = agence;
        this.adresse = adresse;
        this.chambre = chambre;
        this.piece = piece;
        this.dure = dure;
        this.telephone = telephone;
        this.userId = userId;
        this.latLong = latLong;
        this.user = user;
    }


    public String getLouerID() {
        return louerID;
    }

    public String getUserId() {
        return userId;
    }


    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getLouerID(String louerID) {
        return this.louerID;
    }

    public void setLouerID(String louerID) {
        this.louerID = louerID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getMesure() {
        return mesure;
    }

    public void setMesure(String mesure) {
        this.mesure = mesure;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAgence() {
        return agence;
    }

    public void setAgence(String agence) {
        this.agence = agence;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getChambre() {
        return chambre;
    }

    public void setChambre(String chambre) {
        this.chambre = chambre;
    }

    public String getPiece() {
        return piece;
    }

    public void setPiece(String piece) {
        this.piece = piece;
    }

    public String getDure() {
        return dure;
    }

    public void setDure(String dure) {
        this.dure = dure;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getLatLong() {
        return latLong;
    }

    public void setLatLong(String latLong) {
        this.latLong = latLong;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}