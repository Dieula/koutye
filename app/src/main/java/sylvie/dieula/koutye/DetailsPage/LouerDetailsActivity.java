package sylvie.dieula.koutye.DetailsPage;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import sylvie.dieula.koutye.Activities.VenteActivity;
import sylvie.dieula.koutye.Models.Louer;
import sylvie.dieula.koutye.R;

public class LouerDetailsActivity extends AppCompatActivity {


    FirebaseDatabase database;
    DatabaseReference DetailsLouerReference;
    TextView TypeMaison, Lieu, piece, surface, Chambres, Reference, Prix, desciption, teleP;
    ImageView tvDetailsImage, ivMail, ivCall, ivMessage, ivLocalisation;
    CollapsingToolbarLayout collapsing;
    FloatingActionButton btn_favorites;

    String LocationID = "";
    Louer louer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_louer_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


    database = FirebaseDatabase.getInstance();
    DetailsLouerReference = database.getReference("louer");

    btn_favorites = (FloatingActionButton) findViewById(R.id.btn_favorites);


    tvDetailsImage = (ImageView) findViewById(R.id.tvDetailsImage);
  //  collapsing = (CollapsingToolbarLayout) findViewById(R.id.collapsing);

    TypeMaison = (TextView) findViewById(R.id.TypeMaison);
    Lieu = (TextView) findViewById(R.id.Lieu);
    piece = (TextView) findViewById(R.id.piece);
    surface = (TextView) findViewById(R.id.surface);
    Chambres = (TextView) findViewById(R.id.Chambres);
    Reference = (TextView) findViewById(R.id.Reference);
    Prix = (TextView) findViewById(R.id.Prix);
    desciption = (TextView) findViewById(R.id.tvDescription);
    teleP = (TextView) findViewById(R.id.teleP);

    //Imageview
    ivMail = (ImageView) findViewById(R.id.ivMail);
    ivCall = (ImageView) findViewById(R.id.ivCall);
    ivMessage = (ImageView) findViewById(R.id.ivMessage);
    ivLocalisation = (ImageView) findViewById(R.id.ivLocalisation);

    //Listener for button SMS
        ivCall.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            MakeCall();
        }
    });

    //Listener for button Mail
        ivMail.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            SendEMail();
        }
    });

    //Listener for button SMS
        ivMessage.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            ShowDialogSMS();
        }
    });

        if (getIntent() != null) {
        LocationID = getIntent().getStringExtra("louerID");
        getDetailsMaison(LocationID);
    }

}

    //Appel for information
    private void MakeCall() {
        Uri number = Uri.parse("tel:" + louer.getTelephone());
        Intent call = new Intent(Intent.ACTION_CALL, number);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(call);

    }

    //Send email
    private void SendEMail() {

    }

    private void ShowDialogSMS() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(LouerDetailsActivity.this);
        alertDialog.setTitle("    ------ Message------");


        alertDialog.setMessage("---Envoyer votre Message! --");
        LayoutInflater inflater = LouerDetailsActivity.this.getLayoutInflater();
        View addLayout = inflater.inflate(R.layout.layout_sms,null);

        Button btnSend = addLayout.findViewById(R.id.btnSend);

        final EditText tvMessage = addLayout.findViewById(R.id.tvMessage);
        final   TextView tvTelephone = addLayout.findViewById(R.id.teleP);
        tvTelephone.setText(louer.getTelephone());

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String sms = tvMessage.getText().toString();
                String phoneNum = tvTelephone.getText().toString();
                try{
                    SmsManager smsManager = SmsManager.getDefault();
                    smsManager.sendTextMessage(phoneNum, null, sms, null, null);
                    Toast.makeText(LouerDetailsActivity.this, "Message envoyé", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(LouerDetailsActivity.this, VenteActivity.class));
                    finish();
                }catch (Exception e){
                    Toast.makeText(LouerDetailsActivity.this, "erreur", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        });


        alertDialog.setView(addLayout);


        alertDialog.show();
    }


    private void getDetailsMaison(String locationID) {
        DetailsLouerReference.child(locationID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                louer = dataSnapshot.getValue(Louer.class);
                Glide.with(getApplicationContext())
                        .load(louer.getImage())
                        .placeholder(R.drawable.ma)
                        .error(R.drawable.ra)
                        .into(tvDetailsImage);
                Prix.setText("Prix"+louer.getPrice()+"merdeeee");
                piece.setText(louer.getPiece());
                Lieu.setText("Adresse: " + louer.getAdresse());
                TypeMaison.setText("Type de Maison: " +louer.getMesure());
                surface.setText("Mesure: " +louer.getMesure());
                Chambres.setText("Chambres: " +louer.getChambre());
                Reference.setText(louer.getAgence());
                teleP.setText("Téléphone: " +louer.getTelephone());
                //collapsing.setTitle(louer.getDescription());

            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {
                Toast.makeText(LouerDetailsActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // Respond to the action bar's Up/Home button
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
