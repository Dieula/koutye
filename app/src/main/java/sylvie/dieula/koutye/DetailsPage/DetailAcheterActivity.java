package sylvie.dieula.koutye.DetailsPage;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import sylvie.dieula.koutye.Activities.VenteActivity;
import sylvie.dieula.koutye.Common.Common;
import sylvie.dieula.koutye.Models.Acheter;
import sylvie.dieula.koutye.Models.Proprietaire;
import sylvie.dieula.koutye.R;

public class DetailAcheterActivity extends AppCompatActivity {

    private CollapsingToolbarLayout collapsingToolbarLayout;

    FirebaseDatabase database;
    DatabaseReference DetailsAcheterReference;
    TextView TypeMaison,Lieu,piece,surface,Chambres,Reference,Prix,desciption,teleP,tvTelephone,tvMessage;
    ImageView tvDetailsImage,ivMail,IvCall,ivMessage,ivLocalisation,ivCall;
    CollapsingToolbarLayout collapsing;
    FloatingActionButton btn_favorites;

    String AcheterID = "";
    Acheter acheterCurrent;

    Proprietaire proprietaire;

    Acheter serv;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_acheter);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        database= FirebaseDatabase.getInstance();
        DetailsAcheterReference= database.getReference("acheter");

        final FirebaseAuth auth = FirebaseAuth.getInstance();


        String iser_id = auth.getCurrentUser().getUid();

        sharedPreferences = getSharedPreferences("PreferencesTAG", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        tvDetailsImage = (ImageView) findViewById(R.id.tvDetailsImage);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


        //Imageview
        ivMail = (ImageView) findViewById(R.id.ivMail);
        ivMessage = (ImageView) findViewById(R.id.ivMessage);
        ivLocalisation = (ImageView) findViewById(R.id.ivLocalisation);
        ivCall = (ImageView) findViewById(R.id.ivCall);



        ivMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SendMail();
            }
        });

        ivCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MakeCall();
            }
        });

        ivMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowDialogSMS();
            }
        });

        //TExtview
        TypeMaison = (TextView) findViewById(R.id.TypeMaison);
        Lieu = (TextView) findViewById(R.id.Lieu);
        piece = (TextView) findViewById(R.id.piece);
        surface = (TextView) findViewById(R.id.surface);
        Chambres = (TextView) findViewById(R.id.Chambres);
        Reference = (TextView) findViewById(R.id.Reference);
        Prix = (TextView) findViewById(R.id.Prix);
        teleP = (TextView) findViewById(R.id.teleP);
        desciption = (TextView) findViewById(R.id.tvDescription);


        if (getIntent() !=null)
            AcheterID = getIntent().getStringExtra("acheterID");
        if (!AcheterID.isEmpty()){
            if (Common.isConnect(getBaseContext()))
                getDetailsMaison(AcheterID);
            else
            {
                Toast.makeText(this, "PLease check your Connection", Toast.LENGTH_SHORT).show();
                return;
            }
        }

    }

    private void SendMail() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(DetailAcheterActivity.this);
        alertDialog.setTitle("Demander plus d'informations! ");


        // alertDialog.setMessage("Demander plus d'informations!");
        LayoutInflater inflater = DetailAcheterActivity.this.getLayoutInflater();
        View addLayout = inflater.inflate(R.layout.layout_email,null);

        Button btnSend = addLayout.findViewById(R.id.btnSend);

        final EditText tvMessage = addLayout.findViewById(R.id.tvMessage);
        final   TextView tvTelephone = addLayout.findViewById(R.id.teleP);
        final EditText tvNom = addLayout.findViewById(R.id.tvNom);
        final   TextView tvEmail = addLayout.findViewById(R.id.tVEmail);
        //  tvTelephone.setText(acheterCurrent.getTelephone());

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

             /* String nom_client=sharedPreferences.getString("nomAgence", null);
                String telephone_client=sharedPreferences.getString("telephone", null);
                String email_client=sharedPreferences.getString("email", null);

                String uriText = proprietaire.getEmail() +
                        "?subject="+ Uri.encode("Demande d'aide") +
                        "&body="+Uri.encode(
                        "\nNom Client:\t"+nom_client+"\nTelephone Client:\t"+telephone_client+"\nEmail Client:\t"+email_client);
                Uri uri = Uri.parse(uriText);
                Intent send = new Intent(Intent.ACTION_SENDTO);
                send.setData(uri);
                if(send.resolveActivity(getPackageManager()) != null)
                {
                    startActivity(Intent.createChooser(send, "Send email"));

                 }*/
            }
        });


        alertDialog.setView(addLayout);


        alertDialog.show();
    }


    private void MakeCall() {
        Uri number = Uri.parse("tel:" + acheterCurrent.getTelephone());
        Intent call = new Intent(Intent.ACTION_CALL, number);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(call);

    }
    //Sms message
    private void ShowDialogSMS() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(DetailAcheterActivity.this);
        alertDialog.setTitle("    ------ Message------");


        alertDialog.setMessage("--Envoyer votre Message! --");
        LayoutInflater inflater = DetailAcheterActivity.this.getLayoutInflater();
        View addLayout = inflater.inflate(R.layout.layout_sms,null);

        Button btnSend = addLayout.findViewById(R.id.btnSend);

        final EditText tvMessage = addLayout.findViewById(R.id.tvMessage);
        final   TextView tvTelephone = addLayout.findViewById(R.id.teleP);
        tvTelephone.setText(acheterCurrent.getTelephone());

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String sms = tvMessage.getText().toString();
                String phoneNum = tvTelephone.getText().toString();
                try{
                    SmsManager smsManager = SmsManager.getDefault();
                    smsManager.sendTextMessage(phoneNum, null, sms, null, null);
                    Toast.makeText(DetailAcheterActivity.this, "Message envoyé", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(DetailAcheterActivity.this, VenteActivity.class));
                    finish();
                }catch (Exception e){
                    Toast.makeText(DetailAcheterActivity.this, "erreur", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        });


        alertDialog.setView(addLayout);


        alertDialog.show();
    }


    private void getDetailsMaison(String acheterID) {
        DetailsAcheterReference.child(acheterID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                acheterCurrent  = dataSnapshot.getValue(Acheter.class);

                Glide.with(getApplicationContext())
                        .load(acheterCurrent.getImage())
                        .placeholder(R.drawable.ma)
                        .error(R.drawable.ra)
                        .into(tvDetailsImage);

                Prix.setText(" Prix: " + acheterCurrent.getPrice());
                piece.setText("Pièce :" +acheterCurrent.getPiece());
                Lieu.setText(" Adresse: " + acheterCurrent.getAdresse());
                TypeMaison.setText("Type de Maison: " +acheterCurrent.getMesure());
                teleP.setText("Téléphone: " +acheterCurrent.getTelephone());
                surface.setText("Mesure: " +acheterCurrent.getMesure());
                Chambres.setText("Nombre de chambre: " +acheterCurrent.getChambre());
                Reference.setText("Reference: " +acheterCurrent.getAgence());
               // collapsing.setTitle(acheterCurrent.getDescription());
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {
                Toast.makeText(DetailAcheterActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // Respond to the action bar's Up/Home button
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
