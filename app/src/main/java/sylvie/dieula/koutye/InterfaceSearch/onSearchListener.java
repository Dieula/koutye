package sylvie.dieula.koutye.InterfaceSearch;


public interface onSearchListener {
    void onSearch(String query);
    void searchViewOpened();
    void searchViewClosed();
    void onCancelSearch();
}
