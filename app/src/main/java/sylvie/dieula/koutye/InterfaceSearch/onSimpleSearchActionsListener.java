package sylvie.dieula.koutye.InterfaceSearch;


public interface onSimpleSearchActionsListener {
    void onItemClicked(String item);
    void onScroll();
    void error(String localizedMessage);
}
