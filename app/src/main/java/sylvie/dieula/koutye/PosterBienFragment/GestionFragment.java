package sylvie.dieula.koutye.PosterBienFragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.mancj.materialsearchbar.MaterialSearchBar;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import sylvie.dieula.koutye.Activities.KoutyeActivity;
import sylvie.dieula.koutye.Activities.VenteActivity;
import sylvie.dieula.koutye.Models.Acheter;
import sylvie.dieula.koutye.Models.Gestion;
import sylvie.dieula.koutye.Models.Sejour;
import sylvie.dieula.koutye.R;
import sylvie.dieula.koutye.ViewHolder.AcheterViewHolder;
import sylvie.dieula.koutye.ViewHolder.GestionViewHolder;
import sylvie.dieula.koutye.ViewHolder.SejourViewHolder;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class GestionFragment extends Fragment {

    FirebaseStorage storage;
    StorageReference reference;

    //SupportPlaceAutocompleteFragment mAdresse;

    private EditText adresse,montantMin,montantMax,chambre,pieces,Mesure;

    FirebaseDatabase database;
    DatabaseReference GestionReference;

    String gererID = "";

    Uri saveUri;
    private final int Take_Picture = 70;
    Gestion NewKay;
    private TextView PhotoCarte;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_gestion, container, false);

        adresse = v.findViewById(R.id.adresse);
        montantMin = v.findViewById(R.id.montantMin);
        montantMax = v.findViewById(R.id.montantMax);
        chambre = v.findViewById(R.id.chambre);
        Mesure = v.findViewById(R.id.Mesure);
        pieces = v.findViewById(R.id.piece);


        Button btnPost = v.findViewById(R.id.btnPost);
        Button btnCancel = v.findViewById(R.id.btnCancel);
        PhotoCarte = v.findViewById(R.id.PhotoCarte);

        database = FirebaseDatabase.getInstance();
        GestionReference= database.getReference().child("gestion");

        storage = FirebaseStorage.getInstance();
        reference = storage.getReference("images/");


        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               UploadImage();

            }
        });
        //Button connect
        btnPost.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
            @Override
            public void onClick(View view) {
                if (NewKay != null)
                {
                    if(validate())
                    {
                        register();
                        Intent intent=new Intent(getActivity(),VenteActivity.class);
                        startActivity(intent);
                    }
                    else
                    {
                        Toast.makeText(getActivity(), "Please verify form ",Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

        PhotoCarte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TakeImage();
            }
        });





        return v;


    }

    //Save Data
    private void register() {
        String mAdresse= adresse.getText().toString().trim();
        String mChambre = chambre.getText().toString().trim();
        String mPiece = pieces.getText().toString().trim();
        String mMesure = Mesure.getText().toString().trim();

        if (NewKay != null)
        {
            GestionReference.push().setValue(NewKay);
            Toast.makeText(getActivity(), "Adding gestion", Toast.LENGTH_SHORT).show();
            //Snackbar.make(drawer,"New Kay" +NewKay.getAcheterID(AcheterID)+ "Was added",Snackbar.LENGTH_INDEFINITE).show();
        }
    }

    //Verify the fields
    private boolean validate() {
        boolean valid = true;

        String nAdresse= adresse.getText().toString();
        String nChambre = chambre.getText().toString().trim();
        String nPiece = pieces.getText().toString().trim();
        String nMesure = Mesure.getText().toString().trim();
        String nMin= montantMin.getText().toString();
        String nMax= montantMax.getText().toString();


        if (nPiece.isEmpty() || nPiece.length() < 3) {
            pieces.setError("at least 3 characters");
            valid = false;
        } else {
            pieces.setError(null);
        }

        if (nMin.isEmpty() || nMin.length() < 3) {
            montantMin.setError("at least one characters");
            valid = false;
        } else {
            montantMin.setError(null);
        }
        if (nMax.isEmpty() || nMax.length() < 3) {
            montantMax.setError("at least one characters");
            valid = false;
        } else {
            montantMax.setError(null);
        }
        if (nAdresse.isEmpty() || nAdresse.length() < 3) {
            adresse.setError("at least 3 characters");
            valid = false;
        } else {
            adresse.setError(null);
        }

        if (nChambre.isEmpty() || nChambre.length() < 0) {
            chambre.setError("at least one characters");
            valid = false;
        } else {
            chambre.setError(null);
        }

        if (nMesure.isEmpty() || nMesure.length() < 3) {
            Mesure.setError("at least 3 characters");
            valid = false;
        } else {
            Mesure.setError(null);
        }
        return valid;
    }

    private void UploadImage() {
        if(saveUri != null)
        {
            final ProgressDialog progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Loading..");
            progressDialog.show();

            String imageName = UUID.randomUUID().toString();
            final StorageReference PicturePlace = reference.child("images/"+ imageName);
            PicturePlace.putFile(saveUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), "Upload", Toast.LENGTH_SHORT).show();
                            PicturePlace.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    NewKay = new Gestion();
                                    NewKay.setImage(uri.toString());
                                    NewKay.getGererID(gererID);
                                    NewKay.setAdresse(adresse.getText().toString());
                                    NewKay.setChambre(chambre.getText().toString());
                                    NewKay.setMesure(Mesure.getText().toString());
                                    NewKay.setPrice(montantMax.getText().toString());
                                    NewKay.setPiece(pieces.getText().toString());
/*

                                    String.format("%s,%s",houseLocation.getLatLng().latitude,
                                            houseLocation.getLatLng().longitude);
*/

                                }
                            });
                        }}).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressDialog.dismiss();
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    double progress = (100.0  * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                    progressDialog.setMessage("Loadingg " + progress +"%");

                }
            });
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Take_Picture && resultCode == RESULT_OK && data != null && data.getData() != null) {
            saveUri = data.getData();
            PhotoCarte.setText("Image Select!");

        }
    }

    private void TakeImage() {


        Intent i = new Intent();
        i.setType("image/*");
        i.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(i,"Veuillez choisir votre image"), Take_Picture);
    }
}
