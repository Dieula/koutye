package sylvie.dieula.koutye.PosterBienFragment;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;

import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.location.places.ui.SupportPlaceAutocompleteFragment;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sylvie.dieula.koutye.Activities.KoutyeActivity;
import sylvie.dieula.koutye.Common.Common;
import sylvie.dieula.koutye.Manifest;
import sylvie.dieula.koutye.Models.Acheter;
import sylvie.dieula.koutye.Models.Sejour;
import sylvie.dieula.koutye.R;
import sylvie.dieula.koutye.Remote.MyApiEndpointInterface;
import sylvie.dieula.koutye.ViewHolder.AcheterViewHolder;
import sylvie.dieula.koutye.ViewHolder.SejourViewHolder;

import static android.Manifest.permission.CAMERA;
import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class SejourPostFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener{

    FirebaseDatabase database;
    DatabaseReference sejourReference;

    FirebaseRecyclerAdapter<Sejour,SejourViewHolder> adapter;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;

    String SejourID = "";

    Uri saveUri;
    private final int Take_Picture = 70;
    Sejour NewKay;
    private TextView PhotoCarte;


    private Location mLastLocation;
//Location address house
    private LocationRequest locationRequest;
    private GoogleApiClient googleApiClient;
    private Location mlocation;

    private static final int Update = 5000;
    private static final int Fatest = 3000;
    private static final int Displacement = 10;

    private static final int LOCATION_code = 9999;
    private static final  int PLayService = 9997;

   Place houseLocation;

    FirebaseStorage storage;
    StorageReference reference;

    String adress;

    DrawerLayout drawer;

    private EditText adresse,montantMin,montantMax,chambre,pieces,Mesure;

    //Declare the googleMap retrofit
    MyApiEndpointInterface mGoogleMap;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_post_sejour, container, false);

        //init
        mGoogleMap = Common.getMaps();


        adresse = v.findViewById(R.id.adresse);
        montantMin = v.findViewById(R.id.montantMin);
        montantMax = v.findViewById(R.id.montantMax);
        chambre = v.findViewById(R.id.chambre);
        Mesure = v.findViewById(R.id.Mesure);
        pieces = v.findViewById(R.id.piece);

     /*   *//* SupportPlaceAutocompleteFragment mAdresse = (SupportPlaceAutocompleteFragment) getChildFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        //Hide search icon*/
        final PlaceAutocompleteFragment mAdresse = (PlaceAutocompleteFragment) getActivity().getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        //Hide search icon

        Button btnPost = v.findViewById(R.id.btnPost);
        Button btnCancel = v.findViewById(R.id.btnCancel);
        PhotoCarte = v.findViewById(R.id.PhotoCarte);
        RadioButton radioButton = v.findViewById(R.id.radioButton);

        database = FirebaseDatabase.getInstance();
        sejourReference= database.getReference().child("acheter");

        storage = FirebaseStorage.getInstance();
        reference = storage.getReference("images/");

       //Radiobutton for the adress location of the proprietaire
      /*radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
               //get the adress
               if(b)
               {
                   mGoogleMap.getAddressName(String.format("http://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&sensor=false")
                   ,mLastLocation.getLatitude(),
                           mLastLocation.getLongitude())
                           .enqueue(new Callback<String>() {
                               @Override
                               public void onResponse(Call<String> call, Response<String> response) {

                                   //if fetch api
                                   try
                                   {

                                       JSONObject jsonObject = new JSONObject(response.toString());
                                       JSONArray resultsArray = jsonObject.getJSONArray("results");
                                       JSONObject firstObject = resultsArray.getJSONObject(0);
                                       adress = firstObject.getString("formatted_address");

                                       //set this address to edtAddress
                                       ((EditText)mAdresse.getView().findViewById(R.id.place_autocomplete_search_input))
                                               .setText(adress);
                                       //set text size

                                   }catch (JSONException e)
                                   {
                                       e.printStackTrace();
                                   }
                               }

                               @Override
                               public void onFailure(Call<String> call, Throwable t) {

                                   Toast.makeText(getActivity(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                               }
                           });
               }
            }
        });

*/
    //  PlaceAutocompleteFragment mAdresse = (PlaceAutocompleteFragment) getActivity().getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
       //Hide search icon
       mAdresse.getView().findViewById(R.id.place_autocomplete_search_button).setVisibility(View.GONE);
        //Hide hint for auto editext
        ((EditText)mAdresse.getView().findViewById(R.id.place_autocomplete_search_input)).setHint("Entrer votre adresse");
        //set text size
        ((EditText)mAdresse.getView().findViewById(R.id.place_autocomplete_search_input)).setTextSize(15);
        //Get the adresse of the house
        mAdresse.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                 houseLocation = place;
            }

            @Override
            public void onError(Status status) {
                Log.d("ERROR",status.getStatusMessage() );
            }
        });

        //Runtime permission
        if (ActivityCompat.checkSelfPermission(getActivity(),android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(),android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(getActivity(),new String[]
                    {
                            android.Manifest.permission.ACCESS_COARSE_LOCATION,
                            android.Manifest.permission.ACCESS_FINE_LOCATION
                    },LOCATION_code);
        }
        else
        {
            if (checkServices())
            {
             buildGoogle();
             createLocation();
            }
        }

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                UploadImage();

            }
        });
        //Button connect
        btnPost.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
            @Override
            public void onClick(View view) {
                if (NewKay != null)
                {
                    if(validate())
                    {
                        register();
                        Intent intent=new Intent(getActivity(),KoutyeActivity.class);
                        startActivity(intent);
                    }
                    else
                    {
                        Toast.makeText(getActivity(), "Please verify form ",Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

        PhotoCarte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TakeImage();
            }
        });



        return  v;
    }
    //Location google place
   private void createLocation() {
        locationRequest = new LocationRequest();
        locationRequest.setInterval(Update);
        locationRequest.setFastestInterval(Fatest);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setSmallestDisplacement(Displacement);
    }

    //Location google place
    private synchronized void buildGoogle() {

        googleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

       googleApiClient.connect();
    }
    //Location google place
    private boolean checkServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (resultCode != ConnectionResult.SUCCESS)
        {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode))
                GooglePlayServicesUtil.getErrorDialog(resultCode,getActivity(),PLayService).show();
            else 
            {
                Toast.makeText(getActivity(), "Not Supported", Toast.LENGTH_SHORT).show();
               // finish();
            }
            return false;
        }
        return true;
    }

    //Save Data
    private void register() {
        String mAdresse= adresse.getText().toString().trim();
        String mChambre = chambre.getText().toString().trim();
        String mPiece = pieces.getText().toString().trim();
        String mMesure = Mesure.getText().toString().trim();
        adress = houseLocation.getAddress().toString();

        if (NewKay != null)
        {
            sejourReference.push().setValue(NewKay);
            Toast.makeText(getActivity(), "Adding Sejour", Toast.LENGTH_SHORT).show();
            //Snackbar.make(drawer,"New Kay" +NewKay.getAcheterID(AcheterID)+ "Was added",Snackbar.LENGTH_INDEFINITE).show();
        }
    }

    /*@RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)*/

    //Verify the fields
    private boolean validate() {
        boolean valid = true;

        String nAdresse= adresse.getText().toString();
        String nChambre = chambre.getText().toString();
        String nPiece = pieces.getText().toString();
        String nMesure = Mesure.getText().toString();
        adress = houseLocation.getAddress().toString();


        if (nAdresse.isEmpty() || nAdresse.length() < 3) {
            adresse.setError("at least 3 characters");
            valid = false;
        } else {
            adresse.setError(null);
        }

        if (nChambre.isEmpty() || nChambre.length() < 0) {
            chambre.setError("at least one characters");
            valid = false;
        } else {
            chambre.setError(null);
        }

        if (nMesure.isEmpty() || nMesure.length() < 3) {
            Mesure.setError("at least 3 characters");
            valid = false;
        } else {
            Mesure.setError(null);
        }
        return valid;
    }

    private void UploadImage() {
        if(saveUri != null)
        {
            final ProgressDialog progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Loading..");
            progressDialog.show();

            String imageName = UUID.randomUUID().toString();
            final StorageReference PicturePlace = reference.child("images/"+ imageName);
            PicturePlace.putFile(saveUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), "Upload", Toast.LENGTH_SHORT).show();
                            PicturePlace.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    NewKay = new Sejour();
                                    NewKay.setImage(uri.toString());
                                    NewKay.getSejourID(SejourID);
                                    NewKay.setLatLong(houseLocation.getAddress().toString());
                                  adress = houseLocation.getAddress().toString();
                                    NewKay.setAdresse(adresse.getText().toString());
                                    NewKay.setChambre(chambre.getText().toString());
                                    NewKay.setMesure(Mesure.getText().toString());
                                    NewKay.setPrice(montantMax.getText().toString());
                                    NewKay.setPiece(pieces.getText().toString());

                                /*    String.format("%s,%s",houseLocation.getLatLng().latitude,
                                            houseLocation.getLatLng().longitude);
*/
                                }
                            });
                        }}).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressDialog.dismiss();
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    double progress = (100.0  * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                    progressDialog.setMessage("Loadingg " + progress +"%");

                }
            });
        }
    }

    ////Location google place
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode)
        {
            case LOCATION_code:
            {
                if (grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    if (checkServices())
                    {
                        buildGoogle();
                        createLocation();
                    }
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Take_Picture && resultCode == RESULT_OK && data != null && data.getData() != null) {
            saveUri = data.getData();
            PhotoCarte.setText("Image Select!");

        }
    }

    private void TakeImage() {


        Intent i = new Intent();
        i.setType("image/*");
        i.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(i,"Veuillez choisir votre image"), Take_Picture);
    }

    //Location google place

    public void onConnected(@Nullable Bundle bundle) {
      displayLocation();
        startLocationUpdate();

    }

    @Override
    public void onConnectionSuspended(int i) {
        googleApiClient.connect();
    }

    private void startLocationUpdate() {
        if (ActivityCompat.checkSelfPermission(getActivity(),android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(),android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient,locationRequest,this);
    }

    private void displayLocation() {
        if (ActivityCompat.checkSelfPermission(getActivity(),android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(),android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            return;
        }
       mlocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (mlocation !=null)
        {
            Log.d("LOCATION","Your location : "+mlocation.getLatitude()+","+mlocation.getLongitude());
        }
        else
        {
            Log.d("LOCATION","Could not get your Location");
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mlocation = location;
        displayLocation();

    }


}
