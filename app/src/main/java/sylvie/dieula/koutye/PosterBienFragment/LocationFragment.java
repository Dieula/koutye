package sylvie.dieula.koutye.PosterBienFragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import android.support.v4.app.Fragment;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.UUID;

import sylvie.dieula.koutye.Activities.KoutyeActivity;
import sylvie.dieula.koutye.Activities.VenteActivity;
import sylvie.dieula.koutye.Models.Acheter;
import sylvie.dieula.koutye.Models.Louer;
import sylvie.dieula.koutye.R;
import sylvie.dieula.koutye.ViewHolder.AcheterViewHolder;
import sylvie.dieula.koutye.ViewHolder.LouerViewHolder;

import static android.app.Activity.RESULT_OK;


public class LocationFragment extends Fragment {

    FirebaseDatabase database;
    DatabaseReference locationReference;

    FirebaseRecyclerAdapter<Louer,LouerViewHolder> adapter;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;

    String LouerID = "";

    Uri saveUri;
    private final int Take_Picture = 70;
    Louer NewKay;
    private TextView PhotoCarte;

  //  private LocationRequest locationRequest

    FirebaseStorage storage;
    StorageReference reference;

    DrawerLayout drawer;

    //SupportPlaceAutocompleteFragment mAdresse;

    private EditText adresse,montantMin,montantMax,chambre,pieces,Mesure;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_location, container, false);

    }
        // Any view setup should occur here.  E.g., view lookups and attaching view listeners.
        @Override
        public void onViewCreated(View view, Bundle savedInstanceState) {

        adresse = view.findViewById(R.id.adresse);
        montantMin = view.findViewById(R.id.montantMin);
        montantMax =view.findViewById(R.id.montantMax);
        chambre = view.findViewById(R.id.chambre);
        Mesure = view.findViewById(R.id.Mesure);
        pieces = view.findViewById(R.id.piece);

        database = FirebaseDatabase.getInstance();
        locationReference= database.getReference().child("louer");




   // SupportPlaceAutocompleteFragment adresse = (SupportPlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.place);

       /* mAdresse = new SupportPlaceAutocompleteFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.place,mAdresse);
        transaction.commit();
*/
        Button btnPost = view.findViewById(R.id.btnPost);
        Button btnCancel = view.findViewById(R.id.btnCancel);
        PhotoCarte = view.findViewById(R.id.PhotoCarte);

        storage = FirebaseStorage.getInstance();
        reference = storage.getReference("images/");
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                UploadImage();

            }
        });
        btnPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NewKay != null)
                {
                    if(validate())
                    {
                        register();
                        Intent intent=new Intent(getActivity(),VenteActivity.class);
                        startActivity(intent);
                    }
                    else
                    {
                        Toast.makeText(getActivity(), "Please verify form ",Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

        PhotoCarte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TakeImage();
            }
        });



    }

    private void register() {
        String mAdresse= adresse.getText().toString().trim();
        String mChambre = chambre.getText().toString().trim();
        String mPiece = pieces.getText().toString().trim();
        String mMesure = Mesure.getText().toString().trim();

        if (NewKay != null)
        {
            locationReference.push().setValue(NewKay);
            Toast.makeText(getActivity(), "Adding Location", Toast.LENGTH_SHORT).show();
            //Snackbar.make(drawer,"New Kay" +NewKay.getAcheterID(AcheterID)+ "Was added",Snackbar.LENGTH_INDEFINITE).show();
        }
    }

    private boolean validate() {
        boolean valid = true;

        String nAdresse= adresse.getText().toString();
        String nChambre = chambre.getText().toString().trim();
        String nPiece = pieces.getText().toString().trim();
        String nMesure = Mesure.getText().toString().trim();
        String nMin= montantMin.getText().toString();
        String nMax= montantMax.getText().toString();


        if (nPiece.isEmpty() || nPiece.length() < 3) {
            pieces.setError("at least 3 characters");
            valid = false;
        } else {
            pieces.setError(null);
        }

        if (nMin.isEmpty() || nMin.length() < 3) {
            montantMin.setError("at least one characters");
            valid = false;
        } else {
            montantMin.setError(null);
        }
        if (nMax.isEmpty() || nMax.length() < 3) {
            montantMax.setError("at least one characters");
            valid = false;
        } else {
            montantMax.setError(null);
        }
        if (nAdresse.isEmpty() || nAdresse.length() < 3) {
            adresse.setError("at least 3 characters");
            valid = false;
        } else {
            adresse.setError(null);
        }

        if (nChambre.isEmpty() || nChambre.length() < 0) {
            chambre.setError("at least one characters");
            valid = false;
        } else {
            chambre.setError(null);
        }

        if (nMesure.isEmpty() || nMesure.length() < 3) {
            Mesure.setError("at least 3 characters");
            valid = false;
        } else {
            Mesure.setError(null);
        }
        return valid;
    }

    private void UploadImage() {
        if(saveUri != null)
        {
            final ProgressDialog progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Loading..");
            progressDialog.show();

            String imageName = UUID.randomUUID().toString();
            final StorageReference PicturePlace = reference.child("images/"+ imageName);
            PicturePlace.putFile(saveUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), "Upload", Toast.LENGTH_SHORT).show();
                            PicturePlace.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    NewKay = new Louer();
                                    NewKay.getLouerID(LouerID);
                                    NewKay.setAdresse(adresse.getText().toString());
                                    NewKay.setChambre(chambre.getText().toString());
                                    NewKay.setMesure(Mesure.getText().toString());
                                    NewKay.setPrice(montantMax.getText().toString());
                                    NewKay.setPiece(pieces.getText().toString());
                                    NewKay.setImage(uri.toString());


                                }
                            });
                        }}).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressDialog.dismiss();
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    double progress = (100.0  * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                    progressDialog.setMessage("Loadingg " + progress +"%");

                }
            });
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Take_Picture && resultCode == RESULT_OK && data != null && data.getData() != null) {
            saveUri = data.getData();
            PhotoCarte.setText("Image Select!");

        }
    }

    private void TakeImage() {


        Intent i = new Intent();
        i.setType("image/*");
        i.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(i,"Veuillez choisir votre image"), Take_Picture);
    }


}
