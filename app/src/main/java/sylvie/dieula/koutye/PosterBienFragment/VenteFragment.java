package sylvie.dieula.koutye.PosterBienFragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.UUID;

import sylvie.dieula.koutye.Activities.VenteActivity;
import sylvie.dieula.koutye.Common.Common;
import sylvie.dieula.koutye.Models.Acheter;
import sylvie.dieula.koutye.Models.Proprietaire;
import sylvie.dieula.koutye.Models.User;
import sylvie.dieula.koutye.R;
import sylvie.dieula.koutye.ViewHolder.AcheterViewHolder;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class VenteFragment extends Fragment {

    FirebaseDatabase database;
    DatabaseReference AcheterReference;

    FirebaseRecyclerAdapter<Acheter,AcheterViewHolder> adapter;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;

    String AcheterID = "";
    String proprieaireID = "";

    Proprietaire proprietaire;

    Uri saveUri;
    private final int Take_Picture = 70;
    Acheter NewKay;
    private TextView PhotoCarte;


    FirebaseStorage storage;
    StorageReference reference;

    DrawerLayout drawer;


    private EditText adresse,montantMin,montantMax,chambre,pieces,Mesure;

    public VenteFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_vente, container, false);

        adresse = v.findViewById(R.id.adresse);
        montantMin = v.findViewById(R.id.montantMin);
        montantMax = v.findViewById(R.id.montantMax);
        chambre = v.findViewById(R.id.chambre);
        Mesure = v.findViewById(R.id.Mesure);
        pieces = v.findViewById(R.id.piece);

        FirebaseAuth auth = FirebaseAuth.getInstance();

/*

        database = FirebaseDatabase.getInstance();
        String iser_id = auth.getCurrentUser().getDisplayName();

*/

        String iser_id = auth.getCurrentUser().getUid();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        AcheterReference=  database.getReference().child("acheter");

        Button btnPost = v.findViewById(R.id.btnPost);
        Button btnCancel = v.findViewById(R.id.btnCancel);
        PhotoCarte = v.findViewById(R.id.PhotoCarte);
        
     /*   database = FirebaseDatabase.getInstance();
        AcheterReference = database.getReference().child("acheter");
*/
        storage = FirebaseStorage.getInstance();
        reference = storage.getReference("images/");
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                UploadImage();

            }
        });
        btnPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NewKay != null)
                {
                    if(validate())
                    {
                        register();
                        Intent intent=new Intent(getActivity(),VenteActivity.class);
                        startActivity(intent);
                    }
                    else
                    {
                        Toast.makeText(getActivity(), "Please verify form ",Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

        PhotoCarte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TakeImage();
            }
        });



       return  v;
    }

    private void register() {
        String mAdresse= adresse.getText().toString().trim();
        String mChambre = chambre.getText().toString().trim();
        String mPiece = pieces.getText().toString().trim();
        String mMesure = Mesure.getText().toString().trim();

        if (NewKay != null)
        {

            AcheterReference.push().setValue(NewKay);

            Toast.makeText(getActivity(), "Adding Vente", Toast.LENGTH_SHORT).show();
//           Snackbar.make(drawer,"New Kay" +NewKay.getAcheterID(AcheterID)+ "Was added",Snackbar.LENGTH_INDEFINITE).show();
        }
    }

    private boolean validate() {
        boolean valid = true;

        String nAdresse= adresse.getText().toString();
        String nChambre = chambre.getText().toString().trim();
        String nPiece = pieces.getText().toString().trim();
        String nMesure = Mesure.getText().toString().trim();
        String nMin= montantMin.getText().toString();
        String nMax= montantMax.getText().toString();

        if (nAdresse.isEmpty() || nAdresse.length() < 3) {
            adresse.setError("at least 3 characters");
            valid = false;
        } else {
            adresse.setError(null);
        }

        if (nChambre.isEmpty() || nChambre.length() < 0) {
            chambre.setError("at least one characters");
            valid = false;
        } else {
            chambre.setError(null);
        }

        if (nMesure.isEmpty() || nMesure.length() < 3) {
            Mesure.setError("at least 3 characters");
            valid = false;
        } else {
            Mesure.setError(null);
        }
        if (nPiece.isEmpty() || nPiece.length() < 3) {
            pieces.setError("at least one characters");
            valid = false;
        } else {
            pieces.setError(null);
        }

        if (nMin.isEmpty() || nMin.length() < 3) {
            montantMin.setError("at least one characters");
            valid = false;
        } else {
            montantMin.setError(null);
        }
        if (nMax.isEmpty() || nMax.length() < 3) {
            montantMax.setError("at least one characters");
            valid = false;
        } else {
            montantMax.setError(null);
        }
        return valid;
    }



    private void UploadImage() {
        if(saveUri != null)
        {
            final ProgressDialog progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Loading..");
            progressDialog.show();

            final String imageName = UUID.randomUUID().toString();
            final StorageReference PicturePlace = reference.child("images/"+ imageName);
            PicturePlace.putFile(saveUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), "Upload", Toast.LENGTH_SHORT).show();
                            PicturePlace.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    String iser_id = FirebaseAuth.getInstance().getCurrentUser().getUid();
                                    NewKay = new Acheter();
                                    NewKay.setUserId(iser_id);
                                    //proprietaire.setProprieaireID(proprieaireID);
                                    NewKay.setImage(uri.toString());
                                    NewKay.getAcheterID(AcheterID);
                                    NewKay.setAdresse(adresse.getText().toString());
                                    NewKay.setChambre(chambre.getText().toString());
                                    NewKay.setMesure(Mesure.getText().toString());
                                    NewKay.setPrice(montantMax.getText().toString());
                                    NewKay.setPiece(pieces.getText().toString());

                             }
                            });
                        }}).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressDialog.dismiss();
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    double progress = (100.0  * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                    progressDialog.setMessage("Loadingg " + progress +"%");

                }
            });
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Take_Picture && resultCode == RESULT_OK && data != null && data.getData() != null) {
            saveUri = data.getData();
            PhotoCarte.setText("Image Select!");

        }
    }

    private void TakeImage() {


     Intent i = new Intent();
        i.setType("image/*");
        i.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(i,"Veuillez choisir votre image"), Take_Picture);
    }
}
