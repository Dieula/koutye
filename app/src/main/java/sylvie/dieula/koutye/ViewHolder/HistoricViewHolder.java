package sylvie.dieula.koutye.ViewHolder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import sylvie.dieula.koutye.Interface.ItemClickListener;
import sylvie.dieula.koutye.R;

public class HistoricViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView imageV;

    private ItemClickListener itemClickListener;

    public HistoricViewHolder(View itemView) {
        super(itemView);
       // imageV = (ImageView) itemView.findViewById(R.id.imageHistoric);

        itemView.setOnClickListener(this);

    }
    public void setOnClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;

    }
    @Override
    public void onClick(View view) {

    }
}
