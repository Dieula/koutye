package sylvie.dieula.koutye.ViewHolder;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import sylvie.dieula.koutye.Interface.ItemClickListener;
import sylvie.dieula.koutye.Models.User;
import sylvie.dieula.koutye.R;

public class AcheterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView tvDescription;
    public TextView tvDetails;
    public ImageView imageV,ivCoeur,ivShare,ivLocalisation;

    private ItemClickListener itemClickListener;

    Context context;
    public AcheterViewHolder(View itemView) {
        super(itemView);

        tvDescription = (TextView)itemView.findViewById(R.id.tvDescription);
        tvDetails = (TextView)itemView.findViewById(R.id.tvType);
        imageV = (ImageView) itemView.findViewById(R.id.imageV);
        ivCoeur = (ImageView) itemView.findViewById(R.id.ivCoeur);
        ivShare = (ImageView) itemView.findViewById(R.id.ivShare);
        ivLocalisation = (ImageView) itemView.findViewById(R.id.ivLocalisation);

        itemView.setOnClickListener(this);
    }


    public void setOnClickListener(ItemClickListener itemClickListener)
    {
        this.itemClickListener = itemClickListener;

        ivCoeur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

       /* ivShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });*/
    }


    @Override
    public void onClick(View view) {
        itemClickListener.onClick(view,getAdapterPosition(),false);
    }
}
