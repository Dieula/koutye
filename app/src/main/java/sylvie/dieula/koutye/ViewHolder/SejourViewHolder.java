package sylvie.dieula.koutye.ViewHolder;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import sylvie.dieula.koutye.Interface.ItemClickListener;
import sylvie.dieula.koutye.R;

public class SejourViewHolder  extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView tvDescription;
    public TextView tvDetails;
    public ImageView imageV,ivCoeur,ivShare,ivLocalisation;

    private ItemClickListener itemClickListener;

    Context context;

    public SejourViewHolder(View itemView) {
        super(itemView);

        tvDescription = (TextView)itemView.findViewById(R.id.tvDescription);
        // tvDetails = (TextView)itemView.findViewById(R.id.tvDetails);
        imageV = (ImageView) itemView.findViewById(R.id.imageV);
        ivCoeur = (ImageView) itemView.findViewById(R.id.ivCoeur);
        ivShare = (ImageView) itemView.findViewById(R.id.ivShare);
        ivLocalisation = (ImageView) itemView.findViewById(R.id.ivLocalisation);

        itemView.setOnClickListener(this);
    }
    public void setOnClickListener(ItemClickListener itemClickListener)
    {
        this.itemClickListener = itemClickListener;

        ivCoeur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addFavorites();

            }
        });

        ivShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Fayber Agency App");
                sendIntent.putExtra(Intent.EXTRA_TEXT, "Pour plus de detail et de contenu vous pouvez télécharger l'application : https://play.google.com/store/apps/details?id=codepath.fayberapp ou \n visitez notre site Web : http://fayberagency.com/");
                sendIntent.setType("text/plain");
                context.startActivity(sendIntent);

            }
        });
    }

    private void addFavorites() {

    }


    @Override
    public void onClick(View view) {
        itemClickListener.onClick(view,getAdapterPosition(),false);
    }
}
