package sylvie.dieula.koutye.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import sylvie.dieula.koutye.Interface.ItemClickListener;
import sylvie.dieula.koutye.R;

public class FavorisViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

public TextView tvDescription;
public ImageView imageV,ivCoeur;

private ItemClickListener itemClickListener;

public FavorisViewHolder(View itemView) {
        super(itemView);

        tvDescription = (TextView)itemView.findViewById(R.id.tvDescription);
        imageV = (ImageView) itemView.findViewById(R.id.ivFavoris);
        ivCoeur = (ImageView) itemView.findViewById(R.id.btn_favorites);


        itemView.setOnClickListener(this);
        }
public void setOnClickListener(ItemClickListener itemClickListener)
        {
        this.itemClickListener = itemClickListener;

        ivCoeur.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View view) {

        }
        });
        }

@Override
public void onClick(View view) {
        itemClickListener.onClick(view,getAdapterPosition(),false);
        }
}


