package sylvie.dieula.koutye.Common;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.FragmentActivity;

import sylvie.dieula.koutye.Models.User;
import sylvie.dieula.koutye.Remote.GoogleServices;
import sylvie.dieula.koutye.Remote.MyApiEndpointInterface;
import sylvie.dieula.koutye.Remote.RetrofitApiClient;

public class Common {
    public static User currentUser;

    public static final String DELETE = "Delete";
    public static final String USER_KEY = "User";
    public static final String PWD_KEY = "Mot_de_passe";

    //Map activity retrofit
    public static final String Google_API_URL = "http://maps.googleapis.com/";


    public static MyApiEndpointInterface getMaps()
    {
        return RetrofitApiClient.getGoogleClient(Google_API_URL)
                .create(MyApiEndpointInterface.class);
    }

    //verify the internet
    public static boolean isConnect(Context context) {

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        {
            if (connectivityManager != null)
            {
                NetworkInfo[] infos = connectivityManager.getAllNetworkInfo();
                   if (infos != null)
                   {
                       for (int i =0;i<infos.length;i++)
                       {
                           if (infos[i].getState() == NetworkInfo.State.CONNECTED)
                               return true;
                       }
                   }

            }
            return false;
        }
    }
}
