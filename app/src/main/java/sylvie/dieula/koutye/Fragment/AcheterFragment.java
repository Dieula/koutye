package sylvie.dieula.koutye.Fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.mancj.materialsearchbar.MaterialSearchBar;

import java.util.ArrayList;
import java.util.List;

import sylvie.dieula.koutye.DetailsPage.DetailAcheterActivity;
import sylvie.dieula.koutye.Common.Common;
import sylvie.dieula.koutye.Database.Database;
import sylvie.dieula.koutye.Interface.ItemClickListener;
import sylvie.dieula.koutye.Models.Acheter;
import sylvie.dieula.koutye.R;
import sylvie.dieula.koutye.ViewHolder.AcheterViewHolder;


/**
 * A simple {@link Fragment} subclass.
 */
public class AcheterFragment extends Fragment {

    FirebaseDatabase database;
    DatabaseReference AcheterReference;

    FirebaseRecyclerAdapter<Acheter,AcheterViewHolder> adapter;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;

    String AcheterID = "";
    String type = "";

    Uri saveUri;
    private final int Take_Picture = 70;
    Acheter NewKay;
    private TextView typedebien;
    FirebaseStorage storage;
    StorageReference reference;

    //SearchBar

    FirebaseRecyclerAdapter<Acheter,AcheterViewHolder> searchAdapter;
    List<String> suggestList = new ArrayList<>();
    MaterialSearchBar SearchBar;
    DrawerLayout drawer;

    //Favorites
    Database LocalDB;

    ProgressBar progress;

    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_acheter, container, false);
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

       FirebaseAuth auth = FirebaseAuth.getInstance();
        progress = (ProgressBar ) view.findViewById(R.id.progress);

        database = FirebaseDatabase.getInstance();
        String iser_id = auth.getCurrentUser().getUid();
        AcheterReference= database.getReference().child("acheter");

        storage = FirebaseStorage.getInstance();
        reference = storage.getReference("images/");

       // modelArrayList = new ArrayList<>();



        adapter = new FirebaseRecyclerAdapter<Acheter, AcheterViewHolder>(
                Acheter.class,
                R.layout.item_bien,
                AcheterViewHolder.class,
                AcheterReference) {

            @Override
            protected void populateViewHolder(final AcheterViewHolder viewHolder, final Acheter model, final int position) {
                viewHolder.tvDetails.setText(model.getType());
                Glide.with(getActivity())
                        .load(model.getImage())
                        .into(viewHolder.imageV);

              // viewHolder.tvDescription.setText(model.getAdresse());

                //click to share the view
                viewHolder.ivShare.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent sendIntent = new Intent();
                        sendIntent.setAction(Intent.ACTION_SEND);
                        sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Koutye");
                        sendIntent.putExtra(Intent.EXTRA_TEXT, "Pour plus de detail et de contenu vous pouvez télécharger l'application : https://play.google.com/store/apps/ ou \n visitez notre site Web : http://");
                        sendIntent.setType("text/plain");
                        startActivity(sendIntent);

                    }
                });

                //click to share the view
                viewHolder.ivCoeur.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getActivity(), "like", Toast.LENGTH_SHORT).show();
                    }
                });
                //click to share the view
                viewHolder.ivLocalisation.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getActivity(), "Localisation", Toast.LENGTH_SHORT).show();
                    }
                });

           /* //Add favorites
               if (LocalDB.isFavoris(adapter.getRef(position).getKey()))
                   viewHolder.ivCoeur.setImageResource(R.drawable.favoris);

               //Click to change the image
               viewHolder.ivCoeur.setOnClickListener(new View.OnClickListener() {
                   @Override
                   public void onClick(View view) {
                       if (LocalDB.isFavoris(adapter.getRef(position).getKey())) {
                         //  LocalDB.addToFavoris(adapter.getRef(position).getKey());
                           viewHolder.ivCoeur.setImageResource(R.drawable.coeur);
                           Toast.makeText(getActivity(), ""+model.getAgence()+"Add", Toast.LENGTH_SHORT).show();
                       }
                       else
                       {
                           LocalDB.cleanFavoris(adapter.getRef(position).getKey());
                           viewHolder.ivCoeur.setImageResource(R.drawable.coeur);
                           Toast.makeText(getActivity(), ""+model.getAgence()+"Add", Toast.LENGTH_SHORT).show();
                       }

                   }
               });*/


                final Acheter Button = model;
                viewHolder.setOnClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {

                        Intent acheterDetails = new Intent(getActivity(),DetailAcheterActivity.class);
                        acheterDetails.putExtra("acheterID",adapter.getRef(position).getKey());
                        startActivity(acheterDetails);
                    }
                });

            }

        };
        //Swipe refresh
         swipeRefreshLayout = view.findViewById(R.id.swipe_layout);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.lvList);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.getLayoutManager().setMeasurementCacheEnabled(false);

        LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(mRecyclerView.getContext(),
                R.anim.layout_down);
        mRecyclerView.setLayoutAnimation(controller);
        //Local Database
        //LocalDB = new Database(getActivity());

        //Search
        SearchBar = (MaterialSearchBar) view.findViewById(R.id.SearchView);
        SearchBar.setHint("Sear by ville");
        SearchBar.setSpeechMode(false);

       LoadSearch();

        SearchBar.setLastSuggestions(suggestList);
        SearchBar.setCardViewElevation(-1);
        SearchBar.addTextChangeListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                List<String> Suggest = new ArrayList<String>();
                for (String search:suggestList)
                {
                    if (search.toLowerCase().contains(SearchBar.getText().toLowerCase()))
                        Suggest.add(search);
                }
                SearchBar.setLastSuggestions(Suggest);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        SearchBar.setOnSearchActionListener(new MaterialSearchBar.OnSearchActionListener() {
            @Override
            public void onSearchStateChanged(boolean enabled) {

                if (!enabled)
                    mRecyclerView.setAdapter(adapter);
            }

            @Override
            public void onSearchConfirmed(CharSequence text) {
                Toast.makeText(getContext(), "I call my search stuff ", Toast.LENGTH_SHORT).show();
                startSearch(text);
            }

            @Override
            public void onButtonClicked(int buttonCode) {

            }
        });
       swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_blue_dark,
                android.R.color.holo_red_light,
                android.R.color.holo_red_dark);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                progress.setVisibility(View.VISIBLE);
                loadMaison();
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        // Checking internet connection
        if (Common.isConnect(getActivity())) {
            progress.setVisibility(View.VISIBLE);
            loadMaison();
        } else
        {
            Toast.makeText(getActivity(), "Check your internet connection", Toast.LENGTH_SHORT).show();
        }



    }


    private void startSearch(CharSequence text)   {

        searchAdapter = new FirebaseRecyclerAdapter<Acheter, AcheterViewHolder>(
                Acheter.class,
                R.layout.item_bien,
                AcheterViewHolder.class,
                AcheterReference.orderByChild("description").equalTo(text.toString())) {
            @Override
            protected void populateViewHolder(AcheterViewHolder viewHolder, Acheter model, int position) {
                Glide.with(getActivity())
                        .load(model.getImage())
                        .asBitmap().override(1000, 700)
                        .into(viewHolder.imageV);
                final Acheter acheter = model;
                viewHolder.setOnClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        //   Toast.makeText(getActivity(), ""+acheter.getDescription(adresse.getText().toString()), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getActivity(),DetailAcheterActivity.class);
                        startActivity(intent);
                        intent.putExtra("AcheterID",adapter.getRef(position).getKey());
                    }
                });
            }
        };

        mRecyclerView.setAdapter(searchAdapter);
        progress.setVisibility(View.GONE);
        adapter.notifyDataSetChanged();

    }

    private void LoadSearch() {
        AcheterReference.orderByChild("acheterID").equalTo(AcheterID)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot postSnapshot:dataSnapshot.getChildren())
                        {
                            Acheter item = postSnapshot.getValue(Acheter.class);
                           suggestList.add(item.getAdresse());
                        }
                        SearchBar.setLastSuggestions(suggestList);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    private void loadMaison() {

        mRecyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        //Animation
        mRecyclerView.getAdapter().notifyDataSetChanged();
        mRecyclerView.scheduleLayoutAnimation();
    }

}
