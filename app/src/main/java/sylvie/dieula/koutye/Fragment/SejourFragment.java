package sylvie.dieula.koutye.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mancj.materialsearchbar.MaterialSearchBar;

import java.util.ArrayList;
import java.util.List;

import sylvie.dieula.koutye.Common.Common;
import sylvie.dieula.koutye.DetailsPage.DetailsSejourActivity;
import sylvie.dieula.koutye.Interface.ItemClickListener;
import sylvie.dieula.koutye.Models.Acheter;
import sylvie.dieula.koutye.Models.Sejour;
import sylvie.dieula.koutye.R;
import sylvie.dieula.koutye.ViewHolder.AcheterViewHolder;
import sylvie.dieula.koutye.ViewHolder.SejourViewHolder;

/**
 * A simple {@link Fragment} subclass.
 */
public class SejourFragment extends Fragment {


    FirebaseDatabase database;
    DatabaseReference SejourReference;

    FirebaseRecyclerAdapter<Sejour,SejourViewHolder> adapter;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;

    //SearchBar

    FirebaseRecyclerAdapter<Acheter,AcheterViewHolder> searchAdapter;
    List<String> suggestList = new ArrayList<>();
    MaterialSearchBar SearchBar;
    DrawerLayout drawer;

    SwipeRefreshLayout swiperefresh;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sejour, container, false);
    }

    // Any view setup should occur here.  E.g., view lookups and attaching view listeners.
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        swiperefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipe_layout);

        database = FirebaseDatabase.getInstance();
        SejourReference= database.getReference().child("sejour");

        mRecyclerView = (RecyclerView) view.findViewById(R.id.lvList);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.getLayoutManager().setMeasurementCacheEnabled(false);

        //Search
        SearchBar = (MaterialSearchBar) view.findViewById(R.id.SearchView);
        SearchBar.setHint("Enter your search");
        SearchBar.setSpeechMode(false);

//        LoadSearch();

        SearchBar.setLastSuggestions(suggestList);
        SearchBar.setCardViewElevation(-1);

        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swiperefresh.setRefreshing(false);
                loadMaison();
            }
        });
        // Configure the refreshing colors
        swiperefresh.setColorSchemeResources(android.R.color.holo_red_light,
                android.R.color.holo_orange_dark,
                android.R.color.holo_red_dark);

        if (Common.isConnect(getActivity())) {
            loadMaison();
        }
        else
        {
            Toast.makeText(getActivity(), "Check your Connection internet!", Toast.LENGTH_SHORT).show();
        }

    }

        private void loadMaison() {
            Toast.makeText(getContext(), "Load maison", Toast.LENGTH_SHORT).show();
            adapter = new FirebaseRecyclerAdapter<Sejour, SejourViewHolder>(
                    Sejour.class,
                    R.layout.item_bien,
                    SejourViewHolder.class,
                    SejourReference) {


                @Override
                protected void populateViewHolder(SejourViewHolder viewHolder, Sejour model, int position) {
                    // viewHolder.tvDetails.setText(model.getPrice());
                    Glide.with(getActivity())
                            .load(model.getImage())
                            .placeholder(R.drawable.ma)
                           // .error(R.drawable.ra)
                            .into(viewHolder.imageV);

                    //click to share the view
                    viewHolder.ivShare.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent sendIntent = new Intent();
                            sendIntent.setAction(Intent.ACTION_SEND);
                            sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Koutye");
                            sendIntent.putExtra(Intent.EXTRA_TEXT, "Pour plus de detail et de contenu vous pouvez télécharger l'application : https://play.google.com/store/apps/ ou \n visitez notre site Web : http://");
                            sendIntent.setType("text/plain");
                            startActivity(sendIntent);

                        }
                    });

                    //click to share the view
                    viewHolder.ivCoeur.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Toast.makeText(getActivity(), "like", Toast.LENGTH_SHORT).show();
                        }
                    });
                    //click to share the view
                    viewHolder.ivLocalisation.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Toast.makeText(getActivity(), "Localisation", Toast.LENGTH_SHORT).show();
                        }
                    });

                    final Sejour Button = model;
                    viewHolder.setOnClickListener(new ItemClickListener() {
                        @Override
                        public void onClick(View view, int position, boolean isLongClick) {
                            Intent louerDetails = new Intent(getActivity(),DetailsSejourActivity.class);
                            louerDetails.putExtra("SejourID",adapter.getRef(position).getKey());
                            startActivity(louerDetails);
                            // Toast.makeText(getActivity(), ""+Button.getDescription(.getText().toString()), Toast.LENGTH_SHORT).show();
                            Toast.makeText(getActivity(), "Clicked", Toast.LENGTH_SHORT).show();
                        }
                    });

                }

            };
            adapter.notifyDataSetChanged();
            mRecyclerView.setAdapter(adapter);
        }
    }


