package sylvie.dieula.koutye.Fragment;

import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mancj.materialsearchbar.MaterialSearchBar;

import java.util.ArrayList;
import java.util.List;

import sylvie.dieula.koutye.Activities.VenteActivity;
import sylvie.dieula.koutye.Common.Common;
import sylvie.dieula.koutye.DetailsPage.LouerDetailsActivity;
import sylvie.dieula.koutye.Interface.ItemClickListener;
import sylvie.dieula.koutye.Models.Gestion;
import sylvie.dieula.koutye.R;
import sylvie.dieula.koutye.ViewHolder.GestionViewHolder;

public class GererActivity extends AppCompatActivity {

    FirebaseDatabase database;
    DatabaseReference GererReference;

    FirebaseRecyclerAdapter<Gestion,GestionViewHolder> adapter;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;

    //SearchBar

    FirebaseRecyclerAdapter<Gestion,GestionViewHolder> searchAdapter;
    List<String> suggestList = new ArrayList<>();
    MaterialSearchBar SearchBar;
    DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gerer);

        /*getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setTitle("Poster votre bien");*/

        database = FirebaseDatabase.getInstance();
        GererReference = database.getReference().child("gerer");

        mRecyclerView = (RecyclerView) findViewById(R.id.lvList);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.getLayoutManager().setMeasurementCacheEnabled(false);

        //Search
        SearchBar = (MaterialSearchBar) findViewById(R.id.SearchView);
        SearchBar.setHint("Enter your search");
        SearchBar.setSpeechMode(false);

//        LoadSearch();

        SearchBar.setLastSuggestions(suggestList);
        SearchBar.setCardViewElevation(-1);


        if (Common.isConnect(this)) {
            getMaisonGestion();
        }
        else
        {
            Toast.makeText(getApplicationContext(), "Check your Connection internet!", Toast.LENGTH_SHORT).show();
        }

    }

    private void getMaisonGestion() {
        adapter = new FirebaseRecyclerAdapter<Gestion, GestionViewHolder>(
                Gestion.class,
                R.layout.item_bien,
                GestionViewHolder.class,
                GererReference) {

            @Override
            protected void populateViewHolder(GestionViewHolder viewHolder, Gestion model, int position) {
                // viewHolder.tvDetails.setText(model.getPrice());
                Glide.with(getApplication())
                        .load(model.getImage())
                        .placeholder(R.drawable.ma)
                        .into(viewHolder.imageV);

                //click to share the view
                viewHolder.ivShare.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent sendIntent = new Intent();
                        sendIntent.setAction(Intent.ACTION_SEND);
                        sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Koutye");
                        sendIntent.putExtra(Intent.EXTRA_TEXT, "Pour plus de detail et de contenu vous pouvez télécharger l'application : https://play.google.com/store/apps/ ou \n visitez notre site Web : http://");
                        sendIntent.setType("text/plain");
                        startActivity(sendIntent);

                    }
                });

                //click to share the view
                viewHolder.ivCoeur.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getApplication(), "like", Toast.LENGTH_SHORT).show();
                    }
                });
                //click to share the view
                viewHolder.ivLocalisation.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getApplication(), "Localisation", Toast.LENGTH_SHORT).show();
                    }
                });


                final Gestion Button = model;
                viewHolder.setOnClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        // passer class with info
                        Intent louerDetails = new Intent(getApplication(),LouerDetailsActivity.class);
                        louerDetails.putExtra("gererID",adapter.getRef(position).getKey());
                        startActivity(louerDetails);

                    }
                });
            }
        };
        adapter.notifyDataSetChanged();
        mRecyclerView.setAdapter(adapter);
        //  swiperefresh.setRefreshing(false);
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // Respond to the action bar's Up/Home button to the home page with (finishAffinity)
                Intent i = new Intent(GererActivity.this, VenteActivity.class);
                startActivity(i);
                finishAffinity();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
