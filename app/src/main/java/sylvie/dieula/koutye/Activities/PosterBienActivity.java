package sylvie.dieula.koutye.Activities;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import sylvie.dieula.koutye.PosterBienFragment.LocationFragment;
import sylvie.dieula.koutye.PosterBienFragment.SejourPostFragment;
import sylvie.dieula.koutye.PosterBienFragment.VenteFragment;
import sylvie.dieula.koutye.R;
import sylvie.dieula.koutye.Utils.ViewPagerAdapter;

public class PosterBienActivity extends AppCompatActivity {

    private ViewPager viewPager;
    MenuItem prevMenuItem;
    private BottomNavigationView navigation;

    LocationFragment locationFragment;
    VenteFragment venteFragment;
    SejourPostFragment sejourPostFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poster_bien);

      /*   *//*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*//*
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Poster votre bien");

*/
        //Initializing viewPager
        viewPager = (ViewPager) findViewById(R.id.viewpager);

         navigation = (BottomNavigationView) findViewById(R.id.btnNavigation);
         navigation.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.btnVente:
                                viewPager.setCurrentItem(0);
                                break;
                            case R.id.btnLocation:
                                viewPager.setCurrentItem(1);
                                break;
                            case R.id.btnSejour:
                                viewPager.setCurrentItem(2);
                                break;

                        }
                        return false;
                    }
                });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (prevMenuItem != null) {
                    prevMenuItem.setChecked(false);
                }
                else
                {
                    navigation.getMenu().getItem(0).setChecked(false);
                }
                Log.d("page", "onPageSelected: "+position);
                navigation.getMenu().getItem(position).setChecked(true);
                prevMenuItem = navigation.getMenu().getItem(position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        setupViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter (getSupportFragmentManager());

        venteFragment=new VenteFragment();
        locationFragment =new LocationFragment();
        sejourPostFragment=new SejourPostFragment();
        adapter.addFragment(venteFragment);
        adapter.addFragment(locationFragment);
        adapter.addFragment(sejourPostFragment);
        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // Respond to the action bar's Up/Home button
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
