package sylvie.dieula.koutye.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

import sylvie.dieula.koutye.Adapter.FavorisAdapter;
import sylvie.dieula.koutye.Database.Database;
import sylvie.dieula.koutye.Models.Favoris;
import sylvie.dieula.koutye.R;

public class FavorisActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;

    FirebaseDatabase database;
    DatabaseReference favoris;

    List<Favoris> favorite = new ArrayList<>();
    FavorisAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favoris);

        database = FirebaseDatabase.getInstance();
        favoris = database.getReference("Favoris");

        recyclerView = (RecyclerView) findViewById(R.id.listFavoris);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

      loadFavoris();
    }

  private void loadFavoris() {
        favorite = new Database(this).getFavoris();
        adapter = new FavorisAdapter(favorite,this);
        recyclerView.setAdapter(adapter);

    }
}
