package sylvie.dieula.koutye.Activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.ui.AccountKitActivity;

import sylvie.dieula.koutye.Authen.InscriptionActivity;
import sylvie.dieula.koutye.Authen.LoginActivity;
import sylvie.dieula.koutye.Authen.RegisterActivity;
import sylvie.dieula.koutye.Authen.UserAuthActivity;
import sylvie.dieula.koutye.Historic.UserProfilActivity;
import sylvie.dieula.koutye.Map.MapBoxActivity;
import sylvie.dieula.koutye.R;

public class SpalshActivity extends MapBoxActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spalsh);

        //account kit
        AccountKit.initialize(this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(getApplicationContext (),UserAuthActivity.class));

            }
        },2000);


    }


}
