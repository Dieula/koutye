package sylvie.dieula.koutye.Activities;

import android.animation.ValueAnimator;
import android.content.Intent;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionMenu;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;
import sylvie.dieula.koutye.Authen.ProprietaireActivity;
import sylvie.dieula.koutye.Fragment.AcheterFragment;
import sylvie.dieula.koutye.Fragment.GererActivity;
import sylvie.dieula.koutye.Fragment.LouerFragment;
import sylvie.dieula.koutye.Fragment.SejourFragment;
import sylvie.dieula.koutye.Historic.UserProfilActivity;
import sylvie.dieula.koutye.R;
import sylvie.dieula.koutye.Utils.ViewPagerAdapter;

public class VenteActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private ViewPager viewPager;
    MenuItem prevMenuItem;
    private BottomNavigationView navigation;
  //  private BottomBar navigation;

    LouerFragment louerFragment;
    AcheterFragment acheterFragment;
    SejourFragment sejourFragment;

    FloatingActionMenu mainmap_fab_menu;
    com.github.clans.fab.FloatingActionButton poster, gestion, favorite;

    private ArrayList<Location> locations;
    private List<ValueAnimator> animators;
    private ValueAnimator valueAnimator;

    TextView Name, Email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_koutye);

        //Firebase Auth
        FirebaseAuth mAuth = FirebaseAuth.getInstance();

        mainmap_fab_menu = findViewById(R.id.mainmap_fab_menu);
        mainmap_fab_menu.setOnMenuButtonLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (valueAnimator != null) {
                    if (valueAnimator.isRunning()) {
                        valueAnimator.end();
                        /*for (Circle c : circles){
                            c.remove();
                 r           circles.remove(c);
                        }*/
                    }
                }
                return true;
            }
        });
        poster = findViewById(R.id.btnPost);
        poster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ProprietaireActivity.class));
                mainmap_fab_menu.toggle(true);
            }
        });


        gestion = findViewById(R.id.btnGestion);
        gestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), GererActivity.class));

               /*GererFragment fragment = new GererFragment();
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.postFrame, fragment)
                        .addToBackStack(null)
                        .commit();
*/
                mainmap_fab_menu.toggle(true);
            }
        });
        favorite = findViewById(R.id.btnFavoris);
        favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(VenteActivity.this, "List Favorites", Toast.LENGTH_SHORT).show();
             //  startActivity(new Intent(getApplicationContext(), FavorisActivity.class));

                mainmap_fab_menu.toggle(true);
            }
        });



       DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);



        if (mAuth.getCurrentUser() != null){
            TextView tv = navigationView.getHeaderView(0).findViewById(R.id.tvName);
            //ImageView imageView = navigationView.getHeaderView(0).findViewById(R.id.nav_header_tv_image);
            TextView textView = navigationView.getHeaderView(0).findViewById(R.id.tvEmail);

            tv.setText(mAuth.getCurrentUser().getDisplayName());
            textView.setText(mAuth.getCurrentUser().getEmail());

        }
     /*
          View nameAgence = navigationView.getHeaderView(0);
        Name = nameAgence.findViewById(R.id.tvName);
        Name.setText(Common.currentUser.getNomAgence());

        View headerView = navigationView.getHeaderView(0);
        Email = headerView.findViewById(R.id.tvEmail);
        Email.setText(Common.currentUser.getEmail());*/

        //Initializing viewPager
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        /*     BottomBar bottomBar = findViewById(R.id.btnNavigation);
        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(int tabId) {
                if (tabId == R.id.home) {
                    viewPager.setCurrentItem(0);
                    // The tab with id R.id.tab_calls was selected,
                    // change your content accordingly.
                } else if (tabId == R.id.partners) {
                    viewPager.setCurrentItem(0);
                    // The tab with id R.id.tab_groups was selected,
                    // change your content accordingly.
                }
                else if (tabId == R.id.transaction)
                {
                    // The tab with id R.id.tab_chats was selected,
                    // change your content accordingly.
                }
                else if (tabId == R.id.account)
                {
                    // The tab with id R.id.tab_chats was selected,
                    // change your content accordingly.
                }
                else if (tabId == R.id.support)
                {
                    startActivity(new Intent(getApplicationContext(), UserProfilActivity.class));
                    // The tab with id R.id.tab_chats was selected,
                    // change your content accordingly.
                }

            }
        });
*/


       navigation = (BottomNavigationView) findViewById(R.id.btnNavigation);
        navigation.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.btnVente:
                                viewPager.setCurrentItem(0);
                                break;
                            case R.id.btnLocation:
                                viewPager.setCurrentItem(1);
                                break;
                            case R.id.btnSejour:
                                viewPager.setCurrentItem(2);
                                break;
                            /*case R.id.btnGest:
                                viewPager.setCurrentItem(2);
                                break;*/


                        }
                        return false;
                    }
                });


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (prevMenuItem != null) {
                    prevMenuItem.setChecked(false);
                }
                else
                {
                   navigation.getMenu().getItem(position).setChecked(false);
                }
                Log.d("page", "onPageSelected: "+position);
                navigation.getMenu().getItem(position).setChecked(true);
                prevMenuItem = navigation.getMenu().getItem(position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        setupViewPager(viewPager);
    }
   public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter (getSupportFragmentManager());
        acheterFragment =new AcheterFragment();
        louerFragment=new LouerFragment();
        sejourFragment=new SejourFragment();
        adapter.addFragment(acheterFragment);
        adapter.addFragment(louerFragment);
        adapter.addFragment(sejourFragment);
        viewPager.setAdapter(adapter);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // Respond to the action bar's Up/Home button
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera)
        {
            // Handle the camera action
        }
        else if (id == R.id.nav_gallery)
        {
            startActivity(new Intent(getApplicationContext(),PosterBienActivity.class));
        }
        else if (id == R.id.nav_slideshow)
        {

        } else if (id == R.id.nav_manage) {

        }
        else if (id == R.id.nav_share)
        {
            Intent i = new Intent(VenteActivity.this, UserProfilActivity.class);
            startActivity(i);
        }
        else if (id == R.id.nav_logout)
        {
            //Destroy password and user
          //  Paper.book().destroy();
            FirebaseAuth.getInstance().signOut();
            /*//Sign in out
            Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);*/
        }
        else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
   /* @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }*/
}
    // setupNavigationView();






















/*
    private void setupNavigationView() {
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.btnNavigation);

        // Select first menu item by default and show Fragment accordingly.
        Menu menu = navigation.getMenu();
        selectFragment(menu.getItem(0));

        // Set action to perform when any menu-item is selected.
        navigation.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        selectFragment(item);
                        return false;
                    }
                });
    }

    protected void selectFragment(MenuItem item) {

        item.setChecked(true);

        switch (item.getItemId()) {
            case R.id.btnVente:
                // Action to perform when Home Menu item is selected.
              pushFragment(new LouerFragment());
                break;

            case R.id.btnSejour:
                // Action to perform when Bag Menu item is selected.
               pushFragment(new SejourFragment());
                break;
            case R.id.btnLocation:
                pushFragment(new AcheterFragment());
                //  startActivity(new Intent(getApplicationContext(),SejourActivity.class));
                break;

        }
    }

    protected void pushFragment(Fragment fragmentVente) {
        if (fragmentVente == null)
            return;

        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager != null) {
            android.support.v4.app.FragmentTransaction ft = fragmentManager.beginTransaction();
            if (ft != null) {
                ft.replace(R.id.FrameKoutye, fragmentVente);
                ft.commit();
            }
        }
    }*/



/*


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment VenteFragment = null;


            switch (item.getItemId()) {
                case R.id.btnVente:
                    VenteFragment = new GestionFragment();
             //   startActivity(new Intent(getApplicationContext(),VenteActivity.class));
                    break;

                case R.id.btnSejour:
                    VenteFragment = new LocationActivity();
                  //  startActivity(new Intent(getApplicationContext(),LocationActivity.class));

                    break;
                case R.id.btnLocation:
                    VenteFragment = new GestionFragment();
                  //  startActivity(new Intent(getApplicationContext(),SejourActivity.class));
                    break;
            }
            return loadFragment(VenteFragment);
        }

        private boolean loadFragment(Fragment fragmentVente) {
            //switching fragment
            if (fragmentVente != null) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.FrameKoutye, fragmentVente)
                        .commit();
                return true;
            }
            return false;
        }

    };

*/
