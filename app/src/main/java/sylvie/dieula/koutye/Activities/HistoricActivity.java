package sylvie.dieula.koutye.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import sylvie.dieula.koutye.Common.Common;
import sylvie.dieula.koutye.Models.Acheter;
import sylvie.dieula.koutye.Models.Historic;
import sylvie.dieula.koutye.Models.Louer;
import sylvie.dieula.koutye.R;
import sylvie.dieula.koutye.ViewHolder.AcheterViewHolder;
import sylvie.dieula.koutye.ViewHolder.HistoricViewHolder;
import sylvie.dieula.koutye.ViewHolder.LouerViewHolder;

public class HistoricActivity extends AppCompatActivity {

    FirebaseDatabase database;
    DatabaseReference HistoricReference;

    FirebaseRecyclerAdapter<Historic,HistoricViewHolder> adapter;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;

    SharedPreferences sharedPreferences ;
    SharedPreferences.Editor editor ;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historic);

      /*  getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
*/
        database = FirebaseDatabase.getInstance();
        HistoricReference= database.getReference().child("historic");

        sharedPreferences = getSharedPreferences("PreferencesTAG", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

    /*    if(sharedPreferences.getString("nomAgence", null).length()>5){
            getSupportActionBar().setTitle("Historique : "+sharedPreferences.getString("nomAgence", null).substring(0,6)+"...");
        }else{
            getSupportActionBar().setTitle("Historique : "+sharedPreferences.getString("nomAgence", null));
        }
*/

        mRecyclerView = (RecyclerView) findViewById(R.id.lvHistoric);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.getLayoutManager().setMeasurementCacheEnabled(false);

       getHistoricUser(Common.currentUser.getNomAgence());

    }

    private void getHistoricUser(String nomAgence) {

    adapter = new FirebaseRecyclerAdapter<Historic, HistoricViewHolder>(
                Historic.class,
                R.layout.item_historic,
                HistoricViewHolder.class,
                HistoricReference.orderByChild("nomAgence").equalTo(nomAgence)) {

            @Override
            protected void populateViewHolder(HistoricViewHolder viewHolder, Historic model, int position) {
                Glide.with(getApplication())
                        .load(model.getImage())
                        .placeholder(R.drawable.ma)
                        .into(viewHolder.imageV);

            }
        };
        adapter.notifyDataSetChanged();
        mRecyclerView.setAdapter(adapter);
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // Respond to the action bar's Up/Home button to the home page with (finishAffinity)
                Intent i = new Intent(HistoricActivity.this, VenteActivity.class);
                startActivity(i);
                finishAffinity();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
