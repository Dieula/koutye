package sylvie.dieula.koutye.Database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.List;

import sylvie.dieula.koutye.Models.Favoris;

public class Database extends SQLiteAssetHelper {

    private static final String DB_NAME = "koutyeDB.db";
    private static final int DB_VER = 1;
    public Database(Context context) {
        super(context, DB_NAME, null, DB_VER);
    }

    public List<Favoris> getFavoris() {
        SQLiteDatabase db = getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        String[] sqlSelect = {"KayCategory","UserPhone","acheterID",
                "louerID","SejourID","Image", "KayName", "ImageFavoris"};
        String sqlTable = "KayFavoris";

        qb.setTables(sqlTable);
        Cursor kout = qb.query(db, sqlSelect, "UserPhone=?",new String[]{}, null, null,
                null, null);

        final List<Favoris> response = new ArrayList<>();
        if (kout.moveToFirst()) {
            do {
                response.add(new Favoris(
                        kout.getString(kout.getColumnIndex("KayCategory")),
                        kout.getString(kout.getColumnIndex("KayName")),
                        kout.getString(kout.getColumnIndex("ImageFavoris")),
                        kout.getString(kout.getColumnIndex("Image")),
                        kout.getString(kout.getColumnIndex("UserPhone")),
                        kout.getString(kout.getColumnIndex("acheterID")),
                        kout.getString(kout.getColumnIndex("gererID")),
                        kout.getString(kout.getColumnIndex("SejourID")),
                        kout.getString(kout.getColumnIndex("louerID"))));
            } while (kout.moveToNext());
        }
        return response;
    }


 /*   public void addToFavoris(Favoris favoris){
        SQLiteDatabase db = getWritableDatabase();
        String query = String.format("INSERT INTO Favoris(favoris)VALUES('%s');", KayCategory);
        db.execSQL(query);*/

    public void addToFavoris(Favoris favoris){
        SQLiteDatabase db = getWritableDatabase();
        String query = String.format("INSERT INTO Favoris (KayCategory,Image,KayName,ImageFavoris,louerID" +
                        ",acheterID,gererID,SejourID,UserPhone) " +
                "VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s');",
                favoris.getKayCategory(),
                favoris.getKayName(),
                favoris.getImageFavoris(),
                favoris.getAcheterID(),
                favoris.getGererID(),
                favoris.getLouerID(),
                favoris.getSejourID(),
                favoris.getUserPhone(),
                favoris.getImage());
        db.execSQL(query);

    }
    public void cleanFavoris(){
        SQLiteDatabase db = getReadableDatabase();
        String query = String.format("DELETE FROM KayFavoris");
        db.execSQL(query);

    }

    public boolean isFavoris(Favoris favoris){
        SQLiteDatabase db = getReadableDatabase();
        String query = String.format("SELECT * FROM Favoris WHERE KayCategory='%s';",favoris);
        Cursor cursor = db.rawQuery(query,null);
        if (cursor.getCount() <= 0)
        {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;

    }
}
