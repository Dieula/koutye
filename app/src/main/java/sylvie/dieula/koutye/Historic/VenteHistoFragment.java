package sylvie.dieula.koutye.Historic;


import android.annotation.SuppressLint;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.Map;

import sylvie.dieula.koutye.Common.Common;
import sylvie.dieula.koutye.Models.Acheter;
import sylvie.dieula.koutye.Models.Historic;
import sylvie.dieula.koutye.Models.Louer;
import sylvie.dieula.koutye.R;
import sylvie.dieula.koutye.ViewHolder.AcheterViewHolder;
import sylvie.dieula.koutye.ViewHolder.LouerViewHolder;


/**
 * A simple {@link Fragment} subclass.
 */
public class VenteHistoFragment extends Fragment {

    FirebaseDatabase database;
    DatabaseReference AcheterReference;

    String AcheterID = "";
    Historic acheterCurrent;


    FirebaseRecyclerAdapter<Historic, ViewHolderHistoric> adapter;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_vente_histo, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {


        ImageView tvHistoricImage =  view.findViewById(R.id.tvDetailsImage);


        FirebaseAuth auth = FirebaseAuth.getInstance();


        database = FirebaseDatabase.getInstance();
      //  String iser_id = auth.getCurrentUser().getUid();
        AcheterReference = database.getReference().child("acheter");

        mRecyclerView = (RecyclerView) view.findViewById(R.id.lvHistoric);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.getLayoutManager().setMeasurementCacheEnabled(false);


        getUserHistoric();

       /* FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference reference = storage.getReference("images/");


        if (getActivity().getIntent() !=null)
            AcheterID = getActivity().getIntent().getStringExtra("acheterID");
        if (!AcheterID.isEmpty()){
            if (Common.isConnect(getActivity()))
                getUserHistoric(AcheterID);
            else
            {
                Toast.makeText(getActivity(), "PLease check your Connection", Toast.LENGTH_SHORT).show();
                return;
            }
        }*/


    }

    private void getUserHistoric() {
        Toast.makeText(getActivity(), "Loaddddd", Toast.LENGTH_SHORT).show();

        adapter = new FirebaseRecyclerAdapter<Historic, ViewHolderHistoric>(
                Historic.class,
                R.layout.item_historic,
                ViewHolderHistoric.class,
                AcheterReference) {


            @Override
            protected void populateViewHolder(ViewHolderHistoric viewHolder, Historic model, int position) {
                Glide.with(getActivity())
                        .load(model.getImage())
                        .asBitmap().override(1000, 700)
                        .placeholder(R.drawable.ma)
                        .into(viewHolder.imageV);
                acheterCurrent.getIdUser();
                acheterCurrent.getIdCategorie();
            //    viewHolder.imageV.setImageURI(Uri.parse(adapter.getRef(position).getKey()));
               // viewHolder.tvAdresse.setText(adapter.getRef(position).getKey());
                String iser_id = FirebaseAuth.getInstance().getCurrentUser().getUid();
               model.setIdUser(iser_id);
                      // acheterCurrent.setIdUser(iser_id);
                       //viewHolder.tvAdresse.setText(model.getIdUser());

            }
        };

        mRecyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
}