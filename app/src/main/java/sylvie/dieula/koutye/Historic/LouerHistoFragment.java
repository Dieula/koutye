package sylvie.dieula.koutye.Historic;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import sylvie.dieula.koutye.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class LouerHistoFragment extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_louer_histo, container, false);

     return v;
    }

}
