package sylvie.dieula.koutye.Historic;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import sylvie.dieula.koutye.Interface.ItemClickListener;
import sylvie.dieula.koutye.R;

public class ViewHolderHistoric  extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView imageV;
    TextView tvAdresse;

    private ItemClickListener itemClickListener;

    Context context;
    public ViewHolderHistoric(View itemView) {
        super(itemView);


        imageV = (ImageView) itemView.findViewById(R.id.imageV);
        tvAdresse = (TextView) itemView.findViewById(R.id.tvAdresse);

        itemView.setOnClickListener(this);
    }


    public void setOnClickListener(ItemClickListener itemClickListener)
    {
        this.itemClickListener = itemClickListener;


    }


    @Override
    public void onClick(View view) {
        itemClickListener.onClick(view,getAdapterPosition(),false);
    }
}

