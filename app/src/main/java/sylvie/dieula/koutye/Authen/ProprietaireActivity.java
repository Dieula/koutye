package sylvie.dieula.koutye.Authen;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import sylvie.dieula.koutye.Activities.PosterBienActivity;
import sylvie.dieula.koutye.Models.Proprietaire;
import sylvie.dieula.koutye.R;

import static android.support.v4.graphics.TypefaceCompatUtil.getTempFile;

public class ProprietaireActivity extends AppCompatActivity {

    private EditText name, email, telephone, adresse, pieceIdentite;
    Button button;

    Uri saveUri;
    private final int Take_Picture = 70;
    FirebaseStorage storage;
    StorageReference reference;

    DatabaseReference proprietaire;

    Proprietaire NewPost;

    String proprietaireID;



    private static final int DEFAULT_MIN_WIDTH_QUALITY = 400;        // min pixels
    private static final String TAG = "ImagePicker";
    private static final String TEMP_IMAGE_NAME = "tempImage";

    public static int minWidthQuality = DEFAULT_MIN_WIDTH_QUALITY;

private String mName;
    private String mTelephone;
    private String mAdresse;
    private String mCin;
    private String mEmail;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proprietaire);

        name = (EditText) findViewById(R.id.tvNom);
        email = (EditText) findViewById(R.id.tvEmail);
        telephone = (EditText) findViewById(R.id.teleP);
        adresse = (EditText) findViewById(R.id.tvAdresse);
        pieceIdentite = (EditText) findViewById(R.id.tvPieceIden);


        sharedPreferences = getSharedPreferences("PreferencesTAG", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        button = (Button) findViewById(R.id.btnSend);

       final FirebaseAuth auth = FirebaseAuth.getInstance();


        String iser_id = auth.getCurrentUser().getUid();
      //  String iser_name = auth.getCurrentUser().getDisplayName();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
         proprietaire=  database.getReference().child("Utilisateur").child("prorietaire").child(iser_id);
        getUserInfo();



        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!name.getText().toString().equalsIgnoreCase(""))
                {
                    if(validate())
                    {
                        SaveUserInformation();
                        startActivity(new Intent(getApplicationContext(),PosterBienActivity.class));
                    }
                    else
                    {
                        Toast.makeText(ProprietaireActivity.this, "Please verify form ",Toast.LENGTH_SHORT).show();
                    }


                }
            }
        });


    }

    private boolean validate() {
        boolean valid = true;

        String nName= name.getText().toString();
        String nEmail= email.getText().toString();
        String nTelephone= telephone.getText().toString();
        String nAdresse= adresse.getText().toString();
        String nPiece= pieceIdentite.getText().toString();

        if (nName.isEmpty() || nName.length() < 6) {
            name.setError("at least 3 characters");
            valid = false;
        } else {
            name.setError(null);
        }
        if (nPiece.isEmpty() || nPiece.length() < 16) {
            pieceIdentite.setError("at least 16 characters");
            valid = false;
        } else {
            pieceIdentite.setError(null);
        }
        //Restriction if the email isn't valid
        if (nEmail.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(nEmail).matches()) {
            email.setError("Please enter a valid email address");
        } else {
            email.setError(null);
        }
        if (nTelephone.isEmpty() || nTelephone.length() <8) {
            telephone.setError("at least 8 characters");
            valid = false;
        } else {
            telephone.setError(null);
        }
        if (nAdresse.isEmpty() || nAdresse.length() < 6) {
            adresse.setError("at least 3 characters");
            valid = false;
        } else {
            adresse.setError(null);
        }
        return valid;
    }

    public void getUserInfo(){
        proprietaire.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists() && dataSnapshot.getChildrenCount()>0)
                {
                    Map<String,Object> map = (Map<String, Object>)dataSnapshot.getValue();
                    if (map.get("name")!=null)
                    {
                        mName= map.get("name").toString();
                        name.setText(mName);
                    }
                    if (map.get("telephone")!=null)
                    {
                        mTelephone= map.get("telephone").toString();
                        telephone.setText(mTelephone);
                    }
                    if (map.get("email")!=null)
                    {
                        mEmail= map.get("email").toString();
                        email.setText(mEmail);
                    }
                    if (map.get("addresse")!=null)
                    {
                        mAdresse= map.get("addresse").toString();
                        adresse.setText(mAdresse);
                    }
                    if (map.get("nif")!=null)
                    {
                        mCin= map.get("nif").toString();
                        pieceIdentite.setText(mCin);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    //save information of proprietaire
    private void SaveUserInformation() {

        mAdresse = adresse.getText().toString().trim();
        mTelephone = telephone.getText().toString().trim();
        mCin = pieceIdentite.getText().toString().trim();
        mEmail = email.getText().toString().trim();
        mName = name.getText().toString().trim();

        Map userInfo = new HashMap();
        userInfo.put("name",mName);
        userInfo.put("addresse",mAdresse);
        userInfo.put("nif",mCin);
        userInfo.put("email",mEmail);
        userInfo.put("telephone",mTelephone);
        proprietaire.updateChildren(userInfo);
        editor.apply();

       // Toast.makeText(ProprietaireActivity.this, "Successs", Toast.LENGTH_SHORT).show();



    }

    @SuppressLint("RestrictedApi")
    public static Intent getPickImageIntent(Context context) {
        Intent chooserIntent = null;

        List<Intent> intentList = new ArrayList<>();

        Intent pickIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takePhotoIntent.putExtra("return-data", true);
        takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(getTempFile(context)));
        intentList = addIntentsToList(context, intentList, pickIntent);
        intentList = addIntentsToList(context, intentList, takePhotoIntent);

        if (intentList.size() > 0) {
            chooserIntent = Intent.createChooser(intentList.remove(intentList.size() - 1),
                    context.getString(R.string.pick_image_intent_text));
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentList.toArray(new Parcelable[]{}));
        }

        return chooserIntent;
    }

    private static List<Intent> addIntentsToList(Context context, List<Intent> list, Intent intent) {
        List<ResolveInfo> resInfo = context.getPackageManager().queryIntentActivities(intent, 0);
        for (ResolveInfo resolveInfo : resInfo) {
            String packageName = resolveInfo.activityInfo.packageName;
            Intent targetedIntent = new Intent(intent);
            targetedIntent.setPackage(packageName);
            list.add(targetedIntent);
        }
        return list;
    }
}       /*button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                proprietaire.child("proprieaireID").push().setValue(proprietaireID)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                NewPost = new Proprietaire();
                                NewPost.getProprieaireID(proprietaireID);
                                NewPost.setAdresse(adresse.getText().toString());
                                NewPost.setEmail(email.getText().toString());
                                NewPost.setNomComplet(name.getText().toString());
                                NewPost.setTelephone(telephone.getText().toString());
                                NewPost.setPiecePhoto(pieceIdentite.getText().toString());/
                              DatabaseReference newRef = proprietaire.child("proprieaireID").push();
                                newRef.setValue(proprietaireID);

                                Toast.makeText(ProprietaireActivity.this, "Success", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                // Write failed
                                Toast.makeText(ProprietaireActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });
    }*/

