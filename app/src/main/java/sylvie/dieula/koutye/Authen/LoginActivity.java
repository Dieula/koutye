package sylvie.dieula.koutye.Authen;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import io.paperdb.Paper;
import sylvie.dieula.koutye.Activities.VenteActivity;
import sylvie.dieula.koutye.Common.Common;
import sylvie.dieula.koutye.R;

public class LoginActivity extends AppCompatActivity {

    private EditText Phone, edPassword;
    private Button btnSignIn,btnSignUp,btnFacebook,btnGoogle;
    ProgressDialog progressDialog;
    private FirebaseAuth mAuth;
    private TextView tvEnregistrer;

CheckBox checkBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edPassword = (EditText) findViewById(R.id.edPassword);
        Phone = (EditText) findViewById(R.id.etPhone);

      //  checkBox = findViewById(R.id.chBox);
      //  Paper.init(this);

       /* // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();
*/
        btnSignIn = (Button) findViewById(R.id. btnSignIn);
       /* btnGoogle = (Button) findViewById(R.id. btnGoogle);
        btnFacebook = (Button) findViewById(R.id. btnFacebook);
       */

       tvEnregistrer = findViewById(R.id.tvEnregistrer);

       tvEnregistrer.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               startActivity(new Intent(getApplicationContext(),RegisterActivity.class));
           }
       });

        FirebaseDatabase database = FirebaseDatabase.getInstance();
       final DatabaseReference utilisateur = database.getReference().child("User");

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Common.isConnect(getBaseContext())) {

                    /*//save user
                    if (checkBox.isChecked())
                    {
                       Paper.book().write(Common.USER_KEY,Phone.getText().toString());
                        Paper.book().write(Common.PWD_KEY,edPassword.getText().toString());
                    }*/
                    utilisateur.addValueEventListener(new ValueEventListener() {

                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            if (dataSnapshot.child(Phone.getText().toString()).exists()) {
                                //Get information for the user
                                sylvie.dieula.koutye.Models.User user = dataSnapshot.child(Phone.getText().toString()).getValue(sylvie.dieula.koutye.Models.User.class);
                                user.setTelephone(Phone.getText().toString());
                                if (user.getPassord().equals(edPassword.getText().toString())) {
                                    Toast.makeText(LoginActivity.this, "Logged In!", Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(getApplicationContext(), VenteActivity.class));
                                } else {
                                    Toast.makeText(LoginActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(LoginActivity.this, "Not exist", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

                }
                else
                {
                    Toast.makeText(LoginActivity.this, "Check your connection", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        });

    }
}
