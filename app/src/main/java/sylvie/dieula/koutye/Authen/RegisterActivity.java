package sylvie.dieula.koutye.Authen;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import sylvie.dieula.koutye.Models.User;
import sylvie.dieula.koutye.R;

public class RegisterActivity extends AppCompatActivity {

    private EditText name,email,telephone,password,confirm;
    Button button;
    ProgressDialog progressDialog;
    TextView tvEnregistrer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference utilisateur = database.getReference().child("User");

        tvEnregistrer = findViewById(R.id.tvEnregistrer);
        tvEnregistrer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),LoginActivity.class));
            }
        });

        name = (EditText) findViewById(R.id.edNom);
      //  email = (EditText) findViewById(R.id.edEmail);
        telephone = (EditText) findViewById(R.id.edTele);
        password = (EditText) findViewById(R.id.edPassword);
        confirm = (EditText) findViewById(R.id.edConfirm);
        button = (Button) findViewById(R.id.btnSignUp);

        button = (Button) findViewById(R.id.btnSignUp);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                utilisateur.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        if (dataSnapshot.child(telephone.getText().toString()).exists())
                        {
                            Toast.makeText(RegisterActivity.this, "Phone already exist", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            User user = new User(name.getText().toString(),
                                   password.getText().toString(),email.getText().toString(),
                                    telephone.getText().toString());
                            utilisateur.child(telephone.getText().toString()).setValue(user);
                            Toast.makeText(RegisterActivity.this, "Successs", Toast.LENGTH_SHORT).show();
                            finish();
                           // startActivity(new Intent(getApplicationContext(),KoutyeActivity.class));
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });
    }
}
