package sylvie.dieula.koutye.Authen;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.lang.reflect.Array;
import java.util.Arrays;

import sylvie.dieula.koutye.Activities.KoutyeActivity;
import sylvie.dieula.koutye.Activities.VenteActivity;
import sylvie.dieula.koutye.R;

public class UserAuthActivity extends AppCompatActivity {

    private EditText Phone, edPassword;
    private Button btnSignIn, btnSignUp, btnFacebook, btnGoogle;
    ProgressDialog progressDialog;
    private FirebaseAuth mAuth;
    private TextView tvEnregistrer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_auth);

        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();

        if (mAuth.getCurrentUser() != null) {
            Log.d("LoginActivity", "User already logged in");
            startActivity(new Intent(UserAuthActivity.this, VenteActivity.class));
            finish();
        }

        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
        edPassword = (EditText) findViewById(R.id.edPassword);
        Phone = (EditText) findViewById(R.id.etPhone);


        btnSignIn = (Button) findViewById(R.id.btnSignIn);
        tvEnregistrer = findViewById(R.id.tvEnregistrer);

        tvEnregistrer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), InscriptionActivity.class));
            }
        });


        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = Phone.getText().toString();
                final String password = edPassword.getText().toString();

                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(getApplicationContext(), "Enter email address!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(getApplicationContext(), "Enter password!", Toast.LENGTH_SHORT).show();
                    return;
                }

                progressBar.setVisibility(View.VISIBLE);


                //authenticate user
                mAuth.signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener(UserAuthActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                // If sign in fails, display a message to the user. If sign in succeeds
                                // the auth state listener will be notified and logic to handle the
                                // signed in user can be handled in the listener.
                                progressBar.setVisibility(View.GONE);
                                if (!task.isSuccessful()) {
                                    // there was an error
                                    if (password.length() < 6) {
                                        edPassword.setError(getString(R.string.minimum_password));
                                    } else {
                                        Toast.makeText(UserAuthActivity.this, getString(R.string.auth_failed), Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    Log.d("LoginActivity", "Logged in.");
                                    Intent intent = new Intent(UserAuthActivity.this, VenteActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            }
                        });
            }
        });
    }

}
       /* FirebaseAuth auth = FirebaseAuth.getInstance();
        if (auth.getCurrentUser() != null)
        {
            //if already loginbut
            if (!FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().isEmpty())
            {
                startActivity(new Intent(getApplicationContext(),KoutyeActivity.class)
                .putExtra("phone",FirebaseAuth
                        .getInstance()
                        .getCurrentUser().getPhoneNumber().isEmpty()));
                finish();
            }
            else
            {
              *//* startActivityForResult(AuthUI.getInstance()
               .createSignInIntentBuilder()
               .setAvailableProviders(Arrays.asList(new AuthUI.IdpConfig
                       .Builder(AuthUI.).build(),));*//*
            }
        }*/
