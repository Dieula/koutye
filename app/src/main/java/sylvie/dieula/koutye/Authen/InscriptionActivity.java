package sylvie.dieula.koutye.Authen;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import sylvie.dieula.koutye.Activities.VenteActivity;
import sylvie.dieula.koutye.Database.Database;
import sylvie.dieula.koutye.R;

public class InscriptionActivity extends AppCompatActivity {

    private EditText name,email,telephone,password,confirm;
    Button button;
    ProgressBar progressBar;
    TextView tvEnregistrer;
    FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);

        auth = FirebaseAuth.getInstance();


        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
        name = (EditText) findViewById(R.id.edNom);
        //  email = (EditText) findViewById(R.id.edEmail);
        telephone = (EditText) findViewById(R.id.edTele);
        password = (EditText) findViewById(R.id.edPassword);
        confirm = (EditText) findViewById(R.id.edConfirm);
        button = (Button) findViewById(R.id.btnSignUp);

        button = (Button) findViewById(R.id.btnSignUp);


        tvEnregistrer = findViewById(R.id.tvEnregistrer);
        tvEnregistrer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),UserAuthActivity.class));
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String mNom = name.getText().toString().trim();
                String email = telephone.getText().toString().trim();
                String mPassword = password.getText().toString().trim();

                //Restriction if the email isn't valid
                if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    Toast.makeText(InscriptionActivity.this, "Enter email address!", Toast.LENGTH_SHORT).show();
                }

               /* if (TextUtils.isEmpty(email)) {
                    Toast.makeText(getApplicationContext(), "Enter email address!", Toast.LENGTH_SHORT).show();
                    return;
                }*/

                if (TextUtils.isEmpty(mPassword)) {
                    Toast.makeText(getApplicationContext(), "Enter password!", Toast.LENGTH_SHORT).show();
                    return;
                }


                if (TextUtils.isEmpty(mNom)) {
                    Toast.makeText(getApplicationContext(), "Enter your full name", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (password.length() < 6) {
                    Toast.makeText(getApplicationContext(), "Password too short, enter minimum 6 characters!", Toast.LENGTH_SHORT).show();
                    return;
                }

                progressBar.setVisibility(View.VISIBLE);
                //create user
                auth.createUserWithEmailAndPassword(email, mPassword)
                        .addOnCompleteListener(InscriptionActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                             //   Toast.makeText(InscriptionActivity.this, "createUserWithEmail:onComplete:" + task.isSuccessful(), Toast.LENGTH_SHORT).show();
                                progressBar.setVisibility(View.GONE);
                                // If sign in fails, display a message to the user. If sign in succeeds
                                // the auth state listener will be notified and logic to handle the
                                // signed in user can be handled in the listener.
                                if (!task.isSuccessful()) {
                                    Toast.makeText(InscriptionActivity.this, "Authentication failed." + task.getException(),
                                            Toast.LENGTH_SHORT).show();
                                }
                                else {
                                    String iser_id = auth.getCurrentUser().getUid();
                                    FirebaseDatabase database = FirebaseDatabase.getInstance();
                                    DatabaseReference currentUser=  database.getReference().child("Utilisateur").child("user").child(iser_id);
                                    currentUser.setValue(true);
                                    startActivity(new Intent(InscriptionActivity.this, VenteActivity.class));
                                    finish();
                                }
                            }
                        });

            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        //progressBar.setVisibility(View.GONE);
    }
}
