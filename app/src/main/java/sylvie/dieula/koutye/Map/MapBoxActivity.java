package sylvie.dieula.koutye.Map;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.android.gms.maps.MapView;
import com.mapbox.mapboxsdk.Mapbox;

import sylvie.dieula.koutye.R;

public class MapBoxActivity extends AppCompatActivity {

    private MapView mapView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Mapbox.getInstance(this, "pk.eyJ1IjoiZGlldWxhIiwiYSI6ImNqbzRybXAzMjAyd3EzbG4wN2I2ajdzbXMifQ.1XFSrKlbMA-rCzkshmbNUQ");
        setContentView(R.layout.activity_map_box);

        mapView = (MapView) findViewById(R.id.mapView);

    }
}
