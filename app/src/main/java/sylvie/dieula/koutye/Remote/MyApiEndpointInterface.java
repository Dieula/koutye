package sylvie.dieula.koutye.Remote;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface MyApiEndpointInterface {
    @GET
    Call<String> getAddressName(@Url String url, double latitude, double longitude);


}
